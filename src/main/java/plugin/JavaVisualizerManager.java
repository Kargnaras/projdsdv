package plugin;

import backend.Tracer;
import com.intellij.debugger.DebuggerManager;
import com.intellij.debugger.engine.DebugProcess;
import com.intellij.debugger.engine.SuspendContext;
import com.intellij.debugger.engine.managerThread.DebuggerCommand;
import com.intellij.execution.process.ProcessEvent;
import com.intellij.execution.process.ProcessListener;
import com.intellij.execution.ui.RunnerLayoutUi;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.SimpleToolWindowPanel;
import com.intellij.openapi.util.IconLoader;
import com.intellij.openapi.util.Key;
import com.intellij.ui.AncestorListenerAdapter;
import com.intellij.ui.content.Content;
import com.intellij.util.ui.UIUtil;
import com.intellij.xdebugger.XDebugProcess;
import com.intellij.xdebugger.XDebugSession;
import com.intellij.xdebugger.XDebugSessionListener;
import com.sun.jdi.ThreadReference;
import org.jetbrains.annotations.NotNull;
import ui.UICore;
import javax.swing.event.AncestorEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class JavaVisualizerManager implements XDebugSessionListener {

	private static final String CONTENT_ID = "aed.JavaVisualizerContent";

	private XDebugSession debugSession;
	private Project project;
	private Content content;
	private UICore panel;


	JavaVisualizerManager(Project project, XDebugProcess debugProcess) {

		this.debugSession = debugProcess.getSession();
		this.panel = new UICore();
		this.project = project;
		this.content = null;

		debugProcess.getProcessHandler().addProcessListener(new ProcessListener() {

			@Override
			public void startNotified(@NotNull ProcessEvent processEvent) {

				initializeContent();
			}

			@Override
			public void processTerminated(@NotNull ProcessEvent processEvent) {}
			@Override
			public void processWillTerminate(@NotNull ProcessEvent processEvent, boolean b) {}
			@Override
			public void onTextAvailable(@NotNull ProcessEvent processEvent, @NotNull Key key) {}
		});

		debugSession.addSessionListener(this);
	}

	private void initializeContent() {

		panel.addAncestorListener(new AncestorListenerAdapter() {

			public void ancestorAdded(AncestorEvent event) { forceRefreshVisualizer(); }
		});

		final ActionManager actionManager = ActionManager.getInstance();

		ActionToolbar actionToolbar = actionManager.createActionToolbar("JavaVisualizer.VisualizerToolbar", (DefaultActionGroup) actionManager.getAction("JavaVisualizer.VisualizerToolbar"), false);
		actionToolbar.setTargetComponent(panel);

		SimpleToolWindowPanel simpleToolWindowPanel = new SimpleToolWindowPanel(false, true);
		simpleToolWindowPanel.setToolbar(actionToolbar.getComponent());
		simpleToolWindowPanel.setContent(panel);

		RunnerLayoutUi runnerLayoutUi = debugSession.getUI();
		content = runnerLayoutUi.createContent(CONTENT_ID, simpleToolWindowPanel, "Java Visualizer", IconLoader.getIcon("/icons/viz.png"), null);
		content.setCloseable(false);

		content.getComponent().addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {}

			@Override
			public void keyPressed(KeyEvent e) {

				int key = e.getKeyCode();

				panel.moveContent(key == KeyEvent.VK_UP,key == KeyEvent.VK_DOWN,key == KeyEvent.VK_LEFT,key == KeyEvent.VK_RIGHT);
			}

			@Override
			public void keyReleased(KeyEvent e) {

				panel.moveContent(false, false,  false, false);
			}
		});

		UIUtil.invokeLaterIfNeeded(() -> runnerLayoutUi.addContent(content));
	}

	private void forceRefreshVisualizer() {

		try {
			DebugProcess debugProcess = DebuggerManager.getInstance(project).getDebugProcess(debugSession.getDebugProcess().getProcessHandler());

			if (debugProcess != null) {

				debugProcess.getManagerThread().invokeCommand(new DebuggerCommand() {

					@Override
					public void action() { traceAndVisualize(); }

					@Override
					public void commandCancelled() {}
				});
			}
		}
		catch (Exception e) { System.out.println("Unable to force refresh visualizer: " + e); }
	}

	@Override
	public void sessionPaused() {

		if (content == null) { initializeContent(); }

		try {
			if (panel.isShowing()) { traceAndVisualize(); }
		}
		catch (Exception e) { e.printStackTrace(); }
	}

	private void traceAndVisualize() {

		try {
			SuspendContext suspendContext = (SuspendContext) debugSession.getSuspendContext();

			if (suspendContext == null || suspendContext.getThread() == null) { return; }

			ThreadReference thread = suspendContext.getThread().getThreadReference();

			panel.initAndDraw(new Tracer(thread).getTrace());
		}
		catch (Exception e) { e.printStackTrace(); }
	}
}
