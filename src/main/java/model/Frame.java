package model;

import java.util.TreeMap;

public class Frame {

    public final String name_;
    public final String this_;
    public final TreeMap<String, HeapEntity> fields;

    public Frame(String name_, String this_) {

        this.name_  = name_;
        this.this_  = this_;
        this.fields = new TreeMap<>();
    }
}
