package model.pilhas;

import model.HeapEntity;
import model.HeapEntityType;
import support.Consts;
import ui.pilhas.Pilha;
import java.util.Map;
import java.util.TreeMap;

public class HeapPilha extends HeapEntity {

    private long top;

    public HeapPilha(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.PILHA);
        this.top = Consts.nullRef;
        populateFields(fields);
    }

    @Override
    public Pilha makeSketch() {

        return new Pilha(top);
    }

    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet())
            if (field.getKey().equals("noTopo"))
                top = field.getValue().getId();
    }
}
