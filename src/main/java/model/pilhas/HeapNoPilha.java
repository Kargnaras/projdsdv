package model.pilhas;

import model.HeapEntity;
import model.listas.nos.HeapNoListaSimples;
import ui.pilhas.NoPilha;
import java.util.TreeMap;

public class HeapNoPilha extends HeapNoListaSimples {

    public HeapNoPilha(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, fields);
    }

    @Override
    public NoPilha makeSketch() {

        return new NoPilha(ele, seg);
    }
}
