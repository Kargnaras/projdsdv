package model;

import ui.Sketch;
import java.util.TreeMap;

public abstract class HeapEntity {


	protected final long id;
	protected final String signature;
	protected HeapEntityType heapEntityType;

	public HeapEntity(long id, String signature, HeapEntityType heapEntityType) {

		this.id = id;
		this.signature = signature;
		this.heapEntityType = heapEntityType;
	}

	public long getId() {

		return id;
	}

	public String getSignature() {

		return signature;
	}

	public HeapEntityType getHeapEntityType() {

		return heapEntityType;
	}

	public abstract Sketch makeSketch();
	protected abstract void populateFields(TreeMap<String, HeapEntity> fields);
}
