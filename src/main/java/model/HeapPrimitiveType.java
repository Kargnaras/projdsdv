package model;

public enum HeapPrimitiveType {

    NULL,
    VOID,
    BOOLEAN,
    CHAR,
    BYTE,
    SHORT,
    INT,
    LONG,
    FLOAT,
    DOUBLE,
    STRING,
    REFERENCE
}
