package model;

import ui.Sketch;
import java.util.TreeMap;

public class HeapPrimitive extends HeapEntity {

	protected final String stringValue;
	protected final HeapPrimitiveType heapPrimitiveType;

	public HeapPrimitive(long id, String signature, String stringValue, HeapPrimitiveType heapPrimitiveType) {

		super(id, signature, HeapEntityType.PRIMITIVE);
		this.stringValue = stringValue;
		this.heapPrimitiveType = heapPrimitiveType;
	}

	public String getStringValue() {

		return stringValue;
	}

	public HeapPrimitiveType getHeapPrimitiveType() {

		return heapPrimitiveType;
	}

	public boolean isReference() {

		return heapPrimitiveType == HeapPrimitiveType.REFERENCE;
	}

	@Override
	public Sketch makeSketch() { return null; }

	@Override
	protected void populateFields(TreeMap<String, HeapEntity> fields) {}
}
