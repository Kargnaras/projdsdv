package model.filas;

import model.HeapEntity;
import model.HeapEntityType;
import support.Consts;
import ui.filas.Fila;
import java.util.Map;
import java.util.TreeMap;

public class HeapFila extends HeapEntity {

    protected long els;

    public HeapFila(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.FILA);
        this.els = Consts.nullRef;
        populateFields(fields);
    }

    @Override
    public Fila makeSketch() {

        return new Fila(els);
    }

    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet())
            if (field.getKey().equals("elementos"))
                els = field.getValue().getId();
    }
}
