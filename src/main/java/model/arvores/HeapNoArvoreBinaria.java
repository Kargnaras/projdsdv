package model.arvores;

import model.HeapEntity;
import model.HeapEntityType;
import support.Consts;
import ui.arvores.NoArvoreBinaria;
import java.util.Map;
import java.util.TreeMap;

public class HeapNoArvoreBinaria extends HeapEntity {

    private long esq;
    private long ele;
    private long dir;

    public HeapNoArvoreBinaria(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.NO_ARVORE_BINARIA);
        this.esq = Consts.nullRef;
        this.ele = Consts.nullRef;
        this.dir = Consts.nullRef;
        populateFields(fields);
    }

    @Override
    public NoArvoreBinaria makeSketch() {

        return new NoArvoreBinaria(ele, esq, dir);
    }

    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            switch (field.getKey()) {

                case "esquerda":
                    esq = field.getValue().getId();
                    break;
                case "elemento":
                    ele = field.getValue().getId();
                    break;
                case "direita":
                    dir = field.getValue().getId();
            }
        }
    }
}
