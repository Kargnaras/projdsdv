package model.arvores;

import model.HeapEntity;
import model.HeapEntityType;
import model.HeapPrimitive;
import support.Consts;
import ui.arvores.ArvoreBinaria;
import java.util.Map;
import java.util.TreeMap;

public class HeapArvoreBinaria extends HeapEntity {

    private long rai;
    private String cri;
    private int num;

    public HeapArvoreBinaria(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.ARVORE_BINARIA);
        this.rai = Consts.nullRef;
        this.cri = "";
        this.num = 0;
        populateFields(fields);
    }

    @Override
    public ArvoreBinaria makeSketch() {

        return new ArvoreBinaria(rai, cri, num);
    }

    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            HeapPrimitive hp;

            switch (field.getKey()) {

                case "raiz":
                    rai = field.getValue().getId();
                    break;
                case "criterio":
                    hp = (HeapPrimitive) field.getValue();
                    String[] strs = hp.getSignature().split("\\.");
                    cri = strs[strs.length - 1];
                    cri = cri.replace("ComparacaoLimite", "CL_");
                    break;
                case "numeroElementos":
                    hp = (HeapPrimitive) field.getValue();
                    num = Integer.parseInt(hp.getStringValue());
                    break;
            }
        }
    }
}
