package model.tabelas;

import model.HeapEntity;
import model.HeapEntityType;
import model.HeapPrimitive;
import support.Consts;
import ui.tabelas.TabelaHashPorSondagem;
import java.util.Map;
import java.util.TreeMap;

public class HeapTabelaHashPorSondagem extends HeapEntity {

    protected long tab;
    protected int inc;
    protected int num;
    protected int ina;

    public HeapTabelaHashPorSondagem(long id, String signature, TreeMap<String, HeapEntity> fields, HeapEntityType heapEntityType) {

        super(id, signature, heapEntityType);
        this.tab = Consts.nullRef;
        this.inc = 0;
        this.num = 0;
        this.ina = 0;
        populateFields(fields);
    }

    @Override
    public TabelaHashPorSondagem makeSketch() {

        return new TabelaHashPorSondagem(heapEntityType.toString(), tab, inc, num, ina);
    }

    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            HeapPrimitive hp = (HeapPrimitive) field.getValue();

            switch (field.getKey()) {

                case "tabela":
                    tab = field.getValue().getId();
                    break;
                case "incremento":
                    inc = Integer.parseInt(hp.getStringValue());
                    break;
                case "numeroElementos":
                    num = Integer.parseInt(hp.getStringValue());
                    break;
                case "numeroElementosInativos":
                    ina = Integer.parseInt(hp.getStringValue());
                    break;
            }
        }
    }
}
