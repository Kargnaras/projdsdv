package model.tabelas;

import model.HeapEntity;
import model.HeapEntityType;
import model.HeapPrimitive;
import support.Consts;
import ui.tabelas.Entrada;
import java.util.Map;
import java.util.TreeMap;

public class HeapEntrada extends HeapEntity {

    private long ass;
    private String ati;

    public HeapEntrada(long id, String signature, TreeMap<String, HeapEntity> fields) {
        super(id, signature, HeapEntityType.ENTRADA);
        this.ass = Consts.nullRef;
        this.ati = "false";
        populateFields(fields);
    }

    @Override
    public Entrada makeSketch() {

        return new Entrada(ass,ati);
    }

    @Override
    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            switch (field.getKey()) {

                case "associacao":
                    ass = field.getValue().getId();
                    break;
                case "ativo":
                    HeapPrimitive hp = (HeapPrimitive) field.getValue();
                    ati = hp.getStringValue();
                    break;
            }
        }
    }

}
