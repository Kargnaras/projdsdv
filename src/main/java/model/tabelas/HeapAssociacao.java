package model.tabelas;

import model.HeapEntity;
import model.HeapEntityType;
import model.HeapPrimitive;
import support.Consts;
import ui.tabelas.Associacao;
import java.util.Map;
import java.util.TreeMap;

public class HeapAssociacao extends HeapEntity {

    protected String cha;
    protected long val;

    public HeapAssociacao(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.ASSOCIACAO);
        this.cha = "";
        this.val = Consts.nullRef;
        populateFields(fields);
    }

    @Override
    public Associacao makeSketch() {

        return new Associacao(cha, val);
    }

    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            switch (field.getKey()) {

                case "chave":
                    HeapPrimitive hp = (HeapPrimitive) field.getValue();
                    cha = hp.getStringValue();
                    break;
                case "valor":
                    val = field.getValue().getId();
                    break;
            }
        }
    }
}
