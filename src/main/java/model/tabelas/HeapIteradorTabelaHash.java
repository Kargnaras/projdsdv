package model.tabelas;

import model.HeapEntity;
import model.HeapEntityType;
import model.HeapPrimitive;
import ui.Sketch;
import ui.tabelas.IteradorTabelaHash;
import java.util.Map;
import java.util.TreeMap;

public class HeapIteradorTabelaHash extends HeapEntity {

    protected int cor;
    protected int pro;

    public HeapIteradorTabelaHash(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.ITERADOR_TABELA_HASH);
        this.cor = -1;
        this.pro = -1;
        populateFields(fields);
    }

    @Override
    public Sketch makeSketch() {

        return new IteradorTabelaHash(cor, pro);
    }

    @Override
    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            HeapPrimitive hp;

            switch (field.getKey()) {

                case "corrente":
                    hp = (HeapPrimitive) field.getValue();
                    cor = Integer.parseInt(hp.getStringValue());
                    break;
                case "proximo":
                    hp = (HeapPrimitive) field.getValue();
                    pro = Integer.parseInt(hp.getStringValue());
                    break;
            }
        }
    }
}
