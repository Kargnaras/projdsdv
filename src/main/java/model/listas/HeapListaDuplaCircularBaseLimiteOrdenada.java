package model.listas;

import model.HeapEntity;
import java.util.TreeMap;
import model.HeapEntityType;
import ui.listas.ListaDuplaCircularBaseLimiteOrdenada;

public class HeapListaDuplaCircularBaseLimiteOrdenada extends HeapListaSimplesCircularBaseLimiteOrdenada {

    public HeapListaDuplaCircularBaseLimiteOrdenada(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.LISTA_DUPLA_CIRCULAR_BASE_LIMITE_ORDENADA);
        populateFields(fields);
    }

    @Override
    public ListaDuplaCircularBaseLimiteOrdenada makeSketch() {

        return new ListaDuplaCircularBaseLimiteOrdenada(bas, num, cri);
    }
}
