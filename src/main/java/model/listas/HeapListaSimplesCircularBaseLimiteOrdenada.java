package model.listas;

import model.HeapEntity;
import java.util.TreeMap;

import model.HeapEntityType;
import ui.listas.ListaSimplesCircularBaseLimiteOrdenada;

public class HeapListaSimplesCircularBaseLimiteOrdenada extends HeapListaSimplesCircularBaseOrdenada {

    public HeapListaSimplesCircularBaseLimiteOrdenada(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.LISTA_SIMPLES_CIRCULAR_BASE_LIMITE_ORDENADA);
        populateFields(fields);
    }

    public HeapListaSimplesCircularBaseLimiteOrdenada(long id, String signature, HeapEntityType heapEntityType) {

        super(id, signature, heapEntityType);
    }

    @Override
    public ListaSimplesCircularBaseLimiteOrdenada makeSketch() {

        return new ListaSimplesCircularBaseLimiteOrdenada(bas, num, cri);
    }
}
