package model.listas.iteradores;

import model.HeapEntity;
import model.HeapEntityType;
import support.Consts;
import ui.Sketch;
import ui.listas.iteradores.IteradorDuplo;
import java.util.Map;
import java.util.TreeMap;

public class HeapIteradorDuplo extends HeapEntity {

    protected long curr;
    protected long antPri;
    protected long segUlt;

    public HeapIteradorDuplo(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.ITERADOR_DUPLO);
        this.curr = Consts.nullRef;
        this.antPri = Consts.nullRef;
        this.segUlt = Consts.nullRef;
        populateFields(fields);
    }

    @Override
    public Sketch makeSketch() {

        return new IteradorDuplo(curr, antPri, segUlt);
    }

    @Override
    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            switch (field.getKey()) {
                case "corrente":
                    curr = field.getValue().getId();
                    break;
                case "anteriorAoPrimeiro":
                    antPri = field.getValue().getId();
                    break;
                case "seguinteAoUltimo":
                    segUlt = field.getValue().getId();
                    break;
            }
        }
    }
}
