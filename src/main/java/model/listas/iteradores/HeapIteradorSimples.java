package model.listas.iteradores;

import model.HeapEntity;
import model.HeapEntityType;
import support.Consts;
import ui.Sketch;
import ui.listas.iteradores.IteradorSimples;
import java.util.Map;
import java.util.TreeMap;

public class HeapIteradorSimples extends HeapEntity {

    protected long pro;
    protected long cur;

    public HeapIteradorSimples(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.ITERADOR_SIMPLES);
        this.pro = Consts.nullRef;
        this.cur = Consts.nullRef;
        populateFields(fields);
    }

    @Override
    public Sketch makeSketch() {

        return new IteradorSimples(pro, cur);
    }

    @Override
    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            switch (field.getKey()) {
                case "proximo":
                    pro = field.getValue().getId();
                    break;
                case "currente":
                    cur = field.getValue().getId();
                    break;
            }
        }
    }
}
