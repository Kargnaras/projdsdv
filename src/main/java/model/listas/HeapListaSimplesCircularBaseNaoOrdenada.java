package model.listas;

import java.util.Map;
import model.HeapEntity;
import java.util.TreeMap;

import model.HeapEntityType;
import model.HeapPrimitive;
import support.Consts;
import ui.listas.ListaSimplesCircularBaseNaoOrdenada;

public class HeapListaSimplesCircularBaseNaoOrdenada extends HeapListaSimplesNaoOrdenada {

    protected long bas;

    public HeapListaSimplesCircularBaseNaoOrdenada(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.LISTA_SIMPLES_CIRCULAR_BASE_NAO_ORDENADA);
        this.bas = Consts.nullRef;
        populateFields(fields);
    }

    public HeapListaSimplesCircularBaseNaoOrdenada(long id, String signature, HeapEntityType heapEntityType) {

        super(id, signature, heapEntityType);
        this.bas = Consts.nullRef;
    }

    @Override
    public ListaSimplesCircularBaseNaoOrdenada makeSketch() {

        return new ListaSimplesCircularBaseNaoOrdenada(bas, fin, num);
    }

    @Override
    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            switch (field.getKey()) {

                case "base":
                    bas = field.getValue().getId();
                    break;
                case "noFinal":
                    fin = field.getValue().getId();
                    break;
                case "numeroElementos":
                    HeapPrimitive hp = (HeapPrimitive) field.getValue();
                    num = Integer.parseInt(hp.getStringValue());
                    break;
            }
        }
    }
}
