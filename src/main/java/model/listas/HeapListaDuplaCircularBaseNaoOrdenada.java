package model.listas;

import model.HeapEntity;
import model.HeapEntityType;
import model.HeapPrimitive;
import ui.listas.ListaDuplaCircularBaseNaoOrdenada;
import java.util.Map;
import java.util.TreeMap;

public class HeapListaDuplaCircularBaseNaoOrdenada extends HeapListaSimplesCircularBaseNaoOrdenada {

    public HeapListaDuplaCircularBaseNaoOrdenada(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.LISTA_DUPLA_CIRCULAR_BASE_NAO_ORDENADA);
        populateFields(fields);
    }

    @Override
    public ListaDuplaCircularBaseNaoOrdenada makeSketch() {

        return new ListaDuplaCircularBaseNaoOrdenada(bas, num);
    }

    @Override
    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            switch (field.getKey()) {

                case "base":
                    bas = field.getValue().getId();
                    break;
                case "numeroElementos":
                    HeapPrimitive hp = (HeapPrimitive) field.getValue();
                    num = Integer.parseInt(hp.getStringValue());
                    break;
            }
        }
    }
}
