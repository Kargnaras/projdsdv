package model.listas.nos;

import model.HeapEntity;
import model.HeapEntityType;
import support.Consts;
import ui.Sketch;
import java.util.Map;
import java.util.TreeMap;

public class HeapNoLista extends HeapEntity {

    protected long ant;
    protected long ele;
    protected long seg;

    public HeapNoLista(long id, String signature, HeapEntityType heapEntityType, TreeMap<String, HeapEntity> fields) {

        super(id, signature, heapEntityType);
        this.ant = Consts.nullRef;
        this.ele = Consts.nullRef;
        this.seg = Consts.nullRef;
        populateFields(fields);
    }

    @Override
    public Sketch makeSketch() {

        return null;
    }

    @Override
    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            switch (field.getKey()) {
                case "elemento":
                    ele = field.getValue().getId();
                    break;
                case "anterior":
                    ant = field.getValue().getId();
                    break;
                case "seguinte":
                    seg = field.getValue().getId();
                    break;
            }
        }
    }
}
