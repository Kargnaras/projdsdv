package model.listas.nos;

import model.HeapEntity;
import model.HeapEntityType;
import ui.listas.nos.NoListaSimples;
import java.util.TreeMap;

public class HeapNoListaSimples extends HeapNoLista {

	public HeapNoListaSimples(long id, String signature, TreeMap<String, HeapEntity> fields) {

		super(id, signature, HeapEntityType.NO_SIMPLES, fields);
	}

	@Override
	public NoListaSimples makeSketch() {

		return new NoListaSimples(ele, seg);
	}
}
