package model.listas.nos;

import model.HeapEntity;
import model.HeapEntityType;
import ui.listas.nos.NoListaDupla;
import java.util.TreeMap;

public class HeapNoListaDupla extends HeapNoLista {

    public HeapNoListaDupla(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.NO_DUPLO, fields);
    }

    @Override
    public NoListaDupla makeSketch() {

        return new NoListaDupla(ant, ele, seg);
    }
}
