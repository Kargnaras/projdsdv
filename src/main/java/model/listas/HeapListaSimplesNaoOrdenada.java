package model.listas;

import java.util.Map;
import model.HeapEntity;
import java.util.TreeMap;
import model.HeapEntityType;
import model.HeapPrimitive;
import support.Consts;
import ui.listas.ListaSimplesNaoOrdenada;

public class HeapListaSimplesNaoOrdenada extends HeapEntity {

	protected long ini;
	protected long fin;
	protected int num;

	public HeapListaSimplesNaoOrdenada(long id, String signature, TreeMap<String, HeapEntity> fields) {

		super(id, signature, HeapEntityType.LISTA_SIMPLES_NAO_ORDENADA);
		this.ini = Consts.nullRef;
		this.fin = Consts.nullRef;
		this.num = 0;
		populateFields(fields);
	}

	public HeapListaSimplesNaoOrdenada(long id, String signature, HeapEntityType heapEntityType) {

		super(id, signature, heapEntityType);
		this.ini = Consts.nullRef;
		this.fin = Consts.nullRef;
		this.num = 0;
	}

	@Override
	public ListaSimplesNaoOrdenada makeSketch() {

		return new ListaSimplesNaoOrdenada(ini, fin, num);
	}

	protected void populateFields(TreeMap<String, HeapEntity> fields) {

		for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

			switch (field.getKey()) {

				case "noInicial":
					ini = field.getValue().getId();
					break;
				case "noFinal":
					fin = field.getValue().getId();
					break;
				case "numeroElementos":
					HeapPrimitive hp = (HeapPrimitive) field.getValue();
					num = Integer.parseInt(hp.getStringValue());
					break;
			}
		}
	}
}
