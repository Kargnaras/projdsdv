package model.listas;

import model.HeapEntity;
import java.util.Map;
import java.util.TreeMap;

import model.HeapEntityType;
import model.HeapPrimitive;
import ui.listas.ListaSimplesCircularBaseOrdenada;

public class HeapListaSimplesCircularBaseOrdenada extends HeapListaSimplesCircularBaseNaoOrdenada {

    protected String cri;

    public HeapListaSimplesCircularBaseOrdenada(long id, String signature, TreeMap<String, HeapEntity> fields) {

        super(id, signature, HeapEntityType.LISTA_SIMPLES_CIRCULAR_BASE_ORDENADA);
        this.cri = "";
        populateFields(fields);
    }

    public HeapListaSimplesCircularBaseOrdenada(long id, String signature, HeapEntityType heapEntityType) {

        super(id, signature, heapEntityType);
        this.cri = "";
    }

    @Override
    public ListaSimplesCircularBaseOrdenada makeSketch() {

        return new ListaSimplesCircularBaseOrdenada(bas, num, cri);
    }

    @Override
    protected void populateFields(TreeMap<String, HeapEntity> fields) {

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            HeapPrimitive hp;

            switch (field.getKey()) {

                case "base":
                    bas = field.getValue().getId();
                    break;
                case "numeroElementos":
                    hp = (HeapPrimitive) field.getValue();
                    num = Integer.parseInt(hp.getStringValue());
                    break;
                case "criterio":
                    hp = (HeapPrimitive) field.getValue();
                    String[] strs = hp.getSignature().split("\\.");
                    cri = strs[strs.length - 1];
                    cri = cri.replace("ComparacaoLimite", "CL_");
                    break;
            }
        }
    }
}
