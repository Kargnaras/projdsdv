package model.arrays;

import model.HeapEntity;
import model.HeapEntityType;
import ui.arrays.Array;
import ui.Sketch;
import java.util.TreeMap;

public class HeapArray extends HeapEntity {

    private final HeapEntity[] arr;

    public HeapArray(long id, String signature, HeapEntity[] arr) {

        super(id, signature, HeapEntityType.ARRAY);
        this.arr = arr;
    }

    @Override
    public Sketch makeSketch() {

        return new Array(arr);
    }

    protected void populateFields(TreeMap<String, HeapEntity> fields) {}
}
