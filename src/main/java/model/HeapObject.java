package model;

import ui.Object;
import java.util.TreeMap;

public class HeapObject extends HeapEntity {

	protected final TreeMap<String, HeapEntity> fields;

	public HeapObject(long id, String signature, TreeMap<String, HeapEntity> fields) {

		super(id, signature, HeapEntityType.OBJECT);
		this.fields = fields;
	}

	@Override
	public Object makeSketch() {

		return new Object(fields);
	}

	@Override
	protected void populateFields(TreeMap<String, HeapEntity> fields) {}
}
