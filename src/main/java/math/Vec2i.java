package math;

public class Vec2i {

    public int x;
    public int y;

    public Vec2i() {

        this.x = 0;
        this.y = 0;
    }

    public Vec2i(int x, int y) {

        this.x = x;
        this.y = y;
    }

    public void add(int value) {

        x += value;
        y += value;
    }

    public void sub(int value) {

        x -= value;
        y -= value;
    }

    public void mul(int value) {

        x *= value;
        y *= value;
    }

    public void div(int value) {

        x /= value;
        y /= value;
    }

    public Vec2i addNew(int value) {

        return new Vec2i(x + value, y + value);
    }

    public Vec2i subNew(int value) {

        return new Vec2i(x - value, y - value);
    }

    public Vec2i mulNew(int value) {

        return new Vec2i(x * value, y * value);
    }

    public Vec2i divNew(int value) {

        return new Vec2i(x / value, y / value);
    }

    public void add(Vec2i vec) {

        x += vec.x;
        y += vec.y;
    }

    public void sub(Vec2i vec) {

        x -= vec.x;
        y -= vec.y;
    }

    public void mul(Vec2i vec) {

        x *= vec.x;
        y *= vec.y;
    }

    public void div(Vec2i vec) {

        x /= vec.x;
        y /= vec.y;
    }

    public Vec2i addNew(Vec2i vec) {

        return new Vec2i(x + vec.x, y + vec.y);
    }

    public Vec2i subNew(Vec2i vec) {

        return new Vec2i(x - vec.x, y - vec.y);
    }

    public Vec2i mulNew(Vec2i vec) {

        return new Vec2i(x * vec.x, y * vec.y);
    }

    public Vec2i divNew(Vec2i vec) {

        return new Vec2i(x / vec.x, y / vec.y);
    }

    public float length() {

        return (float) Math.sqrt(x * x + y * y);
    }

    public float lengthSquared() {

        return (float) (x * x + y * y);
    }

    public boolean isZero() {

        return x == 0 && y == 0;
    }

    public void clear() {

        x = 0;
        y = 0;
    }





    public void max(Vec2i vecA) {

        if (this.lengthSquared() < vecA.lengthSquared()) {

            x = vecA.x;
            y = vecA.y;
        }
    }

    public void makeOdd() {

        int addX = (x % 2 == 0 ? 1 : 0);
        int addY = (y % 2 == 0 ? 1 : 0);

        x += addX;
        y += addY;
    }

    public void copy(Vec2i vecA) {

        x = vecA.x;
        y = vecA.y;
    }




    public Vec2i cloneVec() {

        return new Vec2i(x, y);
    }

    public static Vec2i max(Vec2i vecA, Vec2i vecB) {

        if (vecA.lengthSquared() > vecB.lengthSquared()) {
            return vecA;
        }
        return vecB;
    }

    public static boolean areEqual(Vec2i vecA, Vec2i vecB) {

        return vecA.x == vecB.x && vecA.y == vecB.y;
    }

}
