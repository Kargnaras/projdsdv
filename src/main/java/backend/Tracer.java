package backend;

import model.*;
import java.util.*;
import com.sun.jdi.*;
import model.arvores.HeapArvoreBinaria;
import model.arvores.HeapNoArvoreBinaria;
import model.filas.HeapFila;
import model.listas.*;
import model.listas.iteradores.HeapIteradorDuplo;
import model.listas.iteradores.HeapIteradorSimples;
import model.listas.nos.HeapNoListaDupla;
import model.listas.nos.HeapNoListaSimples;
import model.arrays.HeapArray;
import model.pilhas.HeapNoPilha;
import model.pilhas.HeapPilha;
import model.tabelas.HeapAssociacao;
import model.tabelas.HeapEntrada;
import model.tabelas.HeapIteradorTabelaHash;
import model.tabelas.HeapTabelaHashPorSondagem;
import support.Consts;

public class Tracer {

	private final Trace trace;
	private final ThreadReference threadRef;
	private final TreeMap<Long, ObjectReference> converted;
	private final TreeMap<Long, ObjectReference> toConvert;

	public Tracer(ThreadReference threadRef) {

		this.trace = new Trace();
		this.threadRef = threadRef;
		this.converted = new TreeMap<>();
		this.toConvert = new TreeMap<>();
	}

	public Trace getTrace() throws Exception {

		for (StackFrame stackFrame : threadRef.frames())
			if (shouldShowFrame(stackFrame))
				trace.stack.addFirst(toFrame(stackFrame));

		while (toConvert.size() > 0) {

			Map.Entry<Long, ObjectReference> entry = toConvert.firstEntry();
			toConvert.remove(entry.getKey());

			if (!converted.containsKey(entry.getKey())) {
				converted.put(entry.getKey(), entry.getValue());
				trace.heap.put(entry.getKey(), convertObject(entry.getValue()));
			}
		}

		return trace;
	}

	private Frame toFrame(StackFrame sf) throws Exception {

		if (!threadRef.isSuspended())
			throw new IllegalThreadStateException();

		List<LocalVariable> arguments = sf.location().method().arguments();

		String this_ = "this: ----";
		if (sf.thisObject() != null) {
			String[] strings = sf.thisObject().type().signature().split("/");
			this_ = "this: " + strings[strings.length - 1].split(";")[0];
		}

		Frame frame = new Frame((sf.location().method().name() + ":" + sf.location().lineNumber()), this_);

		for (LocalVariable lv : arguments) {
			if (lv.name().equals("args")) {
				Value value = sf.getValue(lv);
				if (!(value instanceof ArrayReference && ((ArrayReference) value).length() == 0))
					frame.fields.put(lv.name(), convertValue(value));
			}
		}

		if (arguments.size() != sf.getArgumentValues().size()) {
			int i = -1;
			for (Value value : sf.getArgumentValues())
				frame.fields.put("anon_arg#" + (++i), convertValue(value));
		}

		List<LocalVariable> variables = sf.location().method().variables();

		if (variables.size() > 0) {
			for (LocalVariable lv : variables) {
				if (!lv.isArgument() && !lv.name().endsWith("$")) {
					try { frame.fields.put(lv.name(), convertValue(sf.getValue(lv))); }
					catch (Exception ignored) {}
				}
			}
		}

		return frame;
	}

	private HeapEntity convertValue(Value value) {

		if (value instanceof BooleanValue) {
			return new HeapPrimitive(value.hashCode(), value.type().signature(), Boolean.toString(((BooleanValue) value).value()), HeapPrimitiveType.BOOLEAN);
		}
		if (value instanceof CharValue) {
			return new HeapPrimitive(value.hashCode(), value.type().signature(), Character.toString(((CharValue) value).value()), HeapPrimitiveType.CHAR);
		}
		if (value instanceof ByteValue) {
			return new HeapPrimitive(value.hashCode(), value.type().signature(), Byte.toString(((ByteValue) value).value()), HeapPrimitiveType.BYTE);
		}
		if (value instanceof ShortValue) {
			return new HeapPrimitive(value.hashCode(), value.type().signature(), Short.toString(((ShortValue) value).value()), HeapPrimitiveType.SHORT);
		}
		if (value instanceof IntegerValue) {
			return new HeapPrimitive(value.hashCode(), value.type().signature(), Integer.toString(((IntegerValue) value).value()), HeapPrimitiveType.INT);
		}
		if (value instanceof LongValue) {
			return new HeapPrimitive(value.hashCode(), value.type().signature(), Long.toString(((LongValue) value).value()), HeapPrimitiveType.LONG);
		}
		if (value instanceof FloatValue) {
			return new HeapPrimitive(value.hashCode(), value.type().signature(), Float.toString(((FloatValue) value).value()), HeapPrimitiveType.FLOAT);
		}
		if (value instanceof DoubleValue) {
			return new HeapPrimitive(value.hashCode(), value.type().signature(), Double.toString(((DoubleValue) value).value()), HeapPrimitiveType.DOUBLE);
		}
		if (value instanceof StringReference) {
			return new HeapPrimitive(value.hashCode(), value.type().signature(), ((StringReference) value).value(), HeapPrimitiveType.STRING);
		}
		if (value instanceof VoidValue) {
			return new HeapPrimitive(Consts.nullRef, "no_signature", String.valueOf(Consts.nullRef), HeapPrimitiveType.VOID);
		}
		if (!(value instanceof ObjectReference)) {
			return new HeapPrimitive(Consts.nullRef, "no_signature", String.valueOf(Consts.nullRef), HeapPrimitiveType.NULL);
		}

		ObjectReference obj = (ObjectReference) value;

		if (obj.referenceType().name().startsWith("java.lang.") && BOXED_TYPES.contains(obj.referenceType().name().substring(10)))
			return convertValue(obj.getValue(obj.referenceType().fieldByName("value")));

		toConvert.put(obj.uniqueID(), obj);

		return new HeapPrimitive(obj.uniqueID(), obj.referenceType().name(), Long.toString(obj.uniqueID()), HeapPrimitiveType.REFERENCE);
	}

	private HeapEntity convertObject(ObjectReference obj) {

		if (obj instanceof ArrayReference) {
			ArrayReference ar = (ArrayReference) obj;
			HeapEntity[] arr = new HeapEntity[ar.length()];
			for (int i = 0; i < ar.length(); i++)
				arr[i] = convertValue(ar.getValue(i));
			return new HeapArray(obj.uniqueID(), obj.type().signature(), arr);
		}

		//--------------------------------------------------------------------------------------------------------------

		TreeMap<String, HeapEntity> out = new TreeMap<>();
		if (shouldShowDetails(obj.referenceType())) {
			Map<Field, Value> fields = obj.getValues(obj.referenceType().visibleFields());
			for (Map.Entry<Field, Value> curr : fields.entrySet())
				if (!curr.getKey().isStatic() && !curr.getKey().isSynthetic())
					out.put(curr.getKey().name(), convertValue(curr.getValue()));
		}

		//--------------------------------------------------------------------------------------------------------------

		if (obj.type().signature().contains("colecoes/iteraveis/lineares")) {

			if (obj.type().signature().contains("naoordenadas/estruturas")) {

				if (obj.type().signature().contains("ListaSimplesNaoOrdenada")) {
					if (obj.type().signature().contains("$No"))
						return new HeapNoListaSimples(obj.uniqueID(), obj.type().signature(), out);
					if (obj.type().signature().contains("$Iterador"))
						return new HeapIteradorSimples(obj.uniqueID(), obj.type().signature(), out);

					return new HeapListaSimplesNaoOrdenada(obj.uniqueID(), obj.type().signature(), out);
				}

				if (obj.type().signature().contains("ListaSimplesCircularBaseNaoOrdenada")) {
					if (obj.type().signature().contains("$No"))
						return new HeapNoListaSimples(obj.uniqueID(), obj.type().signature(), out);
					if (obj.type().signature().contains("$Iterador"))
						return new HeapIteradorSimples(obj.uniqueID(), obj.type().signature(), out);
					return	new HeapListaSimplesCircularBaseNaoOrdenada(obj.uniqueID(), obj.type().signature(), out);
				}

				if (obj.type().signature().contains("ListaDuplaCircularBaseNaoOrdenada")) {
					if (obj.type().signature().contains("$No"))
						return new HeapNoListaDupla(obj.uniqueID(), obj.type().signature(), out);
					if (obj.type().signature().contains("$Iterador"))
						return new HeapIteradorDuplo(obj.uniqueID(), obj.type().signature(), out);
					return new HeapListaDuplaCircularBaseNaoOrdenada(obj.uniqueID(), obj.type().signature(), out);
				}

			}
			else if (obj.type().signature().contains("ordenadas/estruturas")) {
				if (obj.type().signature().contains("ListaSimplesCircularBaseOrdenada")) {
					if (obj.type().signature().contains("$No"))
						return new HeapNoListaSimples(obj.uniqueID(), obj.type().signature(), out);
					if (obj.type().signature().contains("$Iterador"))
						return new HeapIteradorDuplo(obj.uniqueID(), obj.type().signature(), out);
					return	new HeapListaSimplesCircularBaseOrdenada(obj.uniqueID(), obj.type().signature(), out);
				}

				if (obj.type().signature().contains("ListaSimplesCircularBaseLimiteOrdenada")) {
					if (obj.type().signature().contains("$No"))
						return new HeapNoListaSimples(obj.uniqueID(), obj.type().signature(), out);
					if (obj.type().signature().contains("$Iterador"))
						return new HeapIteradorDuplo(obj.uniqueID(), obj.type().signature(), out);
					return	new HeapListaSimplesCircularBaseLimiteOrdenada(obj.uniqueID(), obj.type().signature(), out);
				}

				if (obj.type().signature().contains("ListaDuplaCircularBaseLimiteOrdenada")) {
					if (obj.type().signature().contains("$No"))
						return new HeapNoListaDupla(obj.uniqueID(), obj.type().signature(), out);
					if (obj.type().signature().contains("$Iterador"))
						return new HeapIteradorDuplo(obj.uniqueID(), obj.type().signature(), out);
					return new HeapListaDuplaCircularBaseLimiteOrdenada(obj.uniqueID(), obj.type().signature(), out);
				}

				if (obj.type().signature().contains("ArvoreBinaria")) {
					if (obj.type().signature().contains("$No"))
						return new HeapNoArvoreBinaria(obj.uniqueID(), obj.type().signature(), out);
					return new HeapArvoreBinaria(obj.uniqueID(), obj.type().signature(), out);
				}
			}
		}

		else if (obj.type().signature().contains("colecoes/iteraveis/associativas/estruturas")) {
			if (obj.type().signature().contains("Associacao"))
				return new HeapAssociacao(obj.uniqueID(), obj.type().signature(), out);

			if (obj.type().signature().contains("$Entrada"))
				return new HeapEntrada(obj.uniqueID(), obj.type().signature(), out);

			if (obj.type().signature().contains("$Iterador"))
				return new HeapIteradorTabelaHash(obj.uniqueID(), obj.type().signature(), out);

			if (obj.type().signature().contains("TabelaHashPorSondagemComIncrementoLinear")) {
				return new HeapTabelaHashPorSondagem(obj.uniqueID(), obj.type().signature(), out, HeapEntityType.TABELA_HASH_SONDAGEM_INCREMENTO_LINEAR);
			}

			if (obj.type().signature().contains("TabelaHashPorSondagemComIncrementoPorHash")) {
				return new HeapTabelaHashPorSondagem(obj.uniqueID(), obj.type().signature(), out, HeapEntityType.TABELA_HASH_SONDAGEM_INCREMENTO_HASH);
			}

			if (obj.type().signature().contains("TabelaHashPorSondagemComIncrementoQuadratico")) {
				return new HeapTabelaHashPorSondagem(obj.uniqueID(), obj.type().signature(), out, HeapEntityType.TABELA_HASH_SONDAGEM_INCREMENTO_QUADRATICO);
			}
		}

		if (obj.type().signature().contains("Fila"))
			return new HeapFila(obj.uniqueID(), obj.type().signature(), out);

		if (obj.type().signature().contains("Pilha")) {
			if (obj.type().signature().contains("$No"))
				return new HeapNoPilha(obj.uniqueID(), obj.type().signature(), out);
			return new HeapPilha(obj.uniqueID(), obj.type().signature(), out);
		}

		return new HeapObject(obj.uniqueID(), obj.type().signature(), out);
	}

	private static boolean shouldShowFrame(StackFrame frame) {

		Location loc = frame.location();
		return notInternalPackage(loc.toString()) && !loc.method().name().contains("$access");
	}

	private static boolean notInternalPackage(final String name) {

		return Arrays.stream(INTERNAL_PACKAGES).noneMatch(name::startsWith);
	}

	private static boolean shouldShowDetails(ReferenceType type) {

		return notInternalPackage(type.name());
	}

	private static final List<String> BOXED_TYPES = Arrays.asList("Byte", "Short", "Integer", "Long", "Float", "Double", "Character", "Boolean");
	private static final String[] INTERNAL_PACKAGES = { "java.", "javax.", "sun.", "jdk.", "com.sun.", "com.intellij.", "com.aed.java_visualizer.", "org.junit.", "jh61b.junit.", "jh61b." };
}
