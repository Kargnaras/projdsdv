package backend;

import model.Frame;
import model.HeapEntity;
import java.util.LinkedList;
import java.util.TreeMap;

public class Trace {

	public LinkedList<Frame> stack;
	public TreeMap<Long, HeapEntity> heap;

	public Trace () {

		this.stack = new LinkedList<>();
		this.heap = new TreeMap<>();
	}
}
