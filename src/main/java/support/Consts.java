package support;

import java.awt.*;

public class Consts {

    public static final long nullRef = -1;

    public static final double period       = 1000d / 60d;
    public static final double acceleration = 12.0d;
    public static final double initialSpeed = 5.0d;

    public static final int reason = 6;

    //in pixels
    public static final int radius  = 6;
    public static final int boxSide = 8;

    //in boxes
    public static final int spacing      = 6;
    public static final int spacingBig   = 9;
    public static final int spacingSmall = 3;

    public static final Color blackColor      = new Color(0x000000);
    public static final Color backgroundColor = new Color(0xC8C8C8);

    public static final Color heapColorName = new Color(0x82643B);
    public static final Color heapColorText = new Color(0xC69448);
    public static final Color heapColorRef  = new Color(0xB07200);

    public static final Color stackColorName = new Color(0x105898);
    public static final Color stackColorText = new Color(0x1080BF);
    public static final Color stackColorRef  = new Color(0x104CA0);


    public static final Font fontUI = new Font(Font.SANS_SERIF, Font.PLAIN, 16);
}
