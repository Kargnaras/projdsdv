package support;

import math.Vec2i;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

public class Utils {

    public static long computeKey(int x, int y) {

        return (((long) x) << 32) | ((long) y);
    }

    public static void drawTextRect(Graphics2D g, Color color, String str, int x, int y, int w, int h) {

        g.setColor(color);
        g.fillRect(x, y, w, h);

        g.setColor(Consts.blackColor);
        g.drawRect(x, y, w, h);

        g.setFont(Consts.fontUI);
        g.drawString(str, (x + Consts.spacingSmall * Consts.boxSide / 2), (y + h - Consts.spacingSmall * Consts.boxSide / 2));
    }

    public static void drawRefRect(Graphics2D g, Color color, boolean isRefNull, int x, int y, int w, int h) {

        g.setColor(color);
        g.fillRect(x, y, w, h);

        g.setColor(Consts.blackColor);
        g.drawRect(x, y, w, h);

        if (isRefNull)
            g.fillOval(((x + w / 2) - Consts.radius), ((y + h / 2) - Consts.radius), (Consts.radius * 2), (Consts.radius * 2));
    }

    public static Vec2i getStringDim(Font font, String str) {

        FontRenderContext frc = new FontRenderContext(font.getTransform(), true, true);
        Rectangle r = font.getStringBounds(str, frc).getBounds();

        int w = r.width + Consts.spacingSmall * Consts.boxSide;
        int h = r.height + Consts.spacingSmall * Consts.boxSide;

        w = (w / Consts.boxSide) + (w % Consts.boxSide == 0 ? 0 : 1);
        h = (h / Consts.boxSide) + (h % Consts.boxSide == 0 ? 0 : 1);

        return new Vec2i(w, h);
    }

    public static Path2D makeTriangle(int x, int y, int a, int b) {

        Path2D p = new Path2D.Double();
        p.moveTo(0f, 0f);
        p.lineTo(Consts.boxSide, Consts.boxSide / 2f);
        p.lineTo(0f, Consts.boxSide);
        p.closePath();
        p.transform(AffineTransform.getTranslateInstance(0, -Consts.boxSide / 2f));
        p.transform(AffineTransform.getRotateInstance(a, b));
        p.transform(AffineTransform.getTranslateInstance(x, y));
        return p;
    }
}
