package ui.filas;

import math.Vec2i;
import ui.Sketch;
import ui.listas.ListaDuplaCircularBaseNaoOrdenada;
import ui.world.WorldMap;
import java.awt.*;
import java.util.TreeMap;

public class Fila extends Sketch {

    protected long els;
    protected ListaDuplaCircularBaseNaoOrdenada lis;

    public Fila(long els) {

        super();
        this.els = els;
    }

    @Override
    public void computeSketch(TreeMap<Long, Sketch> heap, WorldMap worldMap, Vec2i tempPos) {

        lis = (ListaDuplaCircularBaseNaoOrdenada) heap.get(els);

        if (!isCompu && !lis.isCompu()) {

            lis.computeSketch(heap, worldMap, tempPos);
            posSketch = lis.getPosSketch();
            dimSketch = lis.getDimSketch();

            isCompu = true;
        }
    }

    @Override
    public void drawSketch(Graphics2D g2d, TreeMap<Long, Sketch> heap, Vec2i tempOffSet) {

        if (!isDrawn && !lis.isDrawn()) {

            lis.drawSketch(g2d, heap, tempOffSet);

            isDrawn = true;
        }
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {}
}
