package ui;

import math.Vec2i;
import support.Consts;
import support.Utils;
import ui.elements.Element;
import ui.world.WorldMap;
import java.awt.*;
import java.util.LinkedList;
import java.util.TreeMap;

public abstract class Sketch {

    protected boolean isDrawn;
    protected boolean isCompu;
    protected Vec2i posSketch;
    protected Vec2i dimSketch;
    protected LinkedList<Element> elements;

    public Sketch() {

        this.isDrawn   = false;
        this.isCompu   = false;
        this.posSketch = new Vec2i();
        this.dimSketch = new Vec2i();
        this.elements  = new LinkedList<>();
    }

    public boolean isDrawn() {

        return isDrawn;
    }

    public boolean isCompu() {

        return isCompu;
    }

    public Vec2i getPosSketch() {

        return posSketch;
    }

    public Vec2i getDimSketch() {

        return dimSketch;
    }

    public void computeSketch(TreeMap<Long, Sketch> heap, WorldMap worldMap, Vec2i tempPos) {

        if (!isCompu) {
            posSketch.copy(tempPos);
            compute(heap, worldMap);
            isCompu = true;
        }
    }

    public void drawSketch(Graphics2D g2d, TreeMap<Long, Sketch> heap, Vec2i tempOffSet) {

        if (!isDrawn) {
            for (Element e : elements) { e.drawElement(g2d, tempOffSet); }
            isDrawn = true;
        }
    }

    protected Vec2i getStringMaxDim(String str) {

        Vec2i out = Utils.getStringDim(Consts.fontUI, str);

        out.makeOdd();

        out.x = Math.max(Consts.spacing, out.x);
        out.y = Math.max(Consts.spacing, out.y);
        out.x = Math.max(out.x, out.y);

        return out;
    }

    protected abstract void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap);
}
