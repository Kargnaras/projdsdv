package ui.elements;

import math.Vec2i;
import support.Consts;

public class StackRefElement extends HeapRefElement {

    public StackRefElement(Vec2i pos, Vec2i dim, long ref, boolean overrideASTAR) {

        super(pos, dim, ref, Consts.stackColorRef, overrideASTAR);
    }
}
