package ui.elements;

import math.Vec2i;
import support.Consts;
import ui.world.WorldMap;

import java.awt.*;

public abstract class Element  {

    protected Vec2i pos;
    protected Vec2i dim;
    protected Color color;

    public Element(Vec2i pos, Vec2i dim) {

        this.pos = pos;
        this.dim = dim;
    }

    public Vec2i getPos() {

        return pos;
    }

    public Vec2i getDim() {

        return dim;
    }

    public Vec2i getCenter() {

        int x = pos.x + dim.x / 2;
        int y = pos.y + dim.y / 2;

        return new Vec2i(x, y);
    }

    public void drawElement(Graphics2D g2d, Vec2i tempOffSet) {

        int x = pos.x * Consts.boxSide + tempOffSet.x;
        int y = pos.y * Consts.boxSide + tempOffSet.y;
        int w = dim.x * Consts.boxSide;
        int h = dim.y * Consts.boxSide;

        draw(g2d, x, y, w, h);
    }

    public abstract void updateWorldMap(WorldMap worldMap);

    protected abstract void draw(Graphics2D g2d, int x, int y, int w, int h);
}
