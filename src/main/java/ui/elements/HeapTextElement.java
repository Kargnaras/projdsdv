package ui.elements;

import math.Vec2i;
import support.Consts;
import support.Utils;
import ui.world.Status;
import ui.world.WorldMap;
import java.awt.*;

public class HeapTextElement extends Element {

    protected String str;

    public HeapTextElement(Vec2i pos, Vec2i dim, String str) {

        super(pos, dim);
        this.str = str;
        this.color = Consts.heapColorText;
    }

    public HeapTextElement(Vec2i pos, Vec2i dim, String str, Color color) {

        super(pos, dim);
        this.str = str;
        this.color = color;
    }

    @Override
    protected void draw(Graphics2D g2d, int x, int y, int w, int h) {

        Utils.drawTextRect(g2d, color, str, x, y, w, h);
    }

    @Override
    public void updateWorldMap(WorldMap worldMap) {

        worldMap.addArea(pos, dim, Status.OCCUPIED);
    }
}
