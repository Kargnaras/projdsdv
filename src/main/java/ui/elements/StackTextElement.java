package ui.elements;

import math.Vec2i;
import support.Consts;

public class StackTextElement extends HeapTextElement {

    public StackTextElement(Vec2i pos, Vec2i dim, String str) {

        super(pos, dim, str, Consts.stackColorText);
    }
}
