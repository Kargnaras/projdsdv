package ui.elements;

import math.Vec2i;
import support.Consts;
import support.Utils;
import ui.world.Status;
import ui.world.WorldMap;
import java.awt.*;

public class HeapNameElement extends Element {

    protected String name;

    public HeapNameElement(Vec2i pos, Vec2i dim, String name) {

        super(pos, dim);
        this.name = name;
        this.color = Consts.heapColorName;
    }

    public HeapNameElement(Vec2i pos, Vec2i dim, String name, Color color) {

        super(pos, dim);
        this.name = name;
        this.color = color;
    }

    @Override
    protected void draw(Graphics2D g2d, int x, int y, int w, int h) {

        Utils.drawTextRect(g2d, color, name, x, y, w, h);
    }

    @Override
    public void updateWorldMap(WorldMap worldMap) {

        worldMap.addArea(pos, dim, Status.OCCUPIED);
    }
}
