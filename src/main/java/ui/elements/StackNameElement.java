package ui.elements;

import math.Vec2i;
import support.Consts;

public class StackNameElement extends HeapNameElement {

    public StackNameElement(Vec2i pos, Vec2i dim, String name) {

        super(pos, dim, name, Consts.stackColorName);
    }
}
