package ui.elements;

import math.Vec2i;
import support.Consts;
import support.Utils;
import ui.world.Status;
import ui.world.WorldMap;
import java.awt.*;

public class HeapRefElement extends Element {

    protected long ref;
    protected boolean overrideASTAR;

    public HeapRefElement(Vec2i pos, Vec2i dim, long ref, boolean overrideASTAR) {

        super(pos, dim);
        this.ref = ref;
        this.color = Consts.heapColorRef;
        this.overrideASTAR = overrideASTAR;
    }

    public HeapRefElement(Vec2i pos, Vec2i dim, long ref, Color color, boolean overrideASTAR) {

        super(pos, dim);
        this.ref = ref;
        this.color = color;
        this.overrideASTAR = overrideASTAR;
    }

    @Override
    protected void draw(Graphics2D g2d, int x, int y, int w, int h) {

        Utils.drawRefRect(g2d, color, (ref == Consts.nullRef), x, y, w, h);
    }

    @Override
    public void updateWorldMap(WorldMap worldMap) {

        worldMap.addPosRef(getCenter(), ref, overrideASTAR);
        worldMap.addArea(pos, dim, Status.REFERENCE);
    }
}
