package ui.arvores;

import math.Vec2i;
import support.Consts;
import ui.Sketch;
import ui.listas.ListaSimplesCircularBaseOrdenada;
import ui.world.WorldMap;
import java.awt.*;
import java.util.TreeMap;

public class ArvoreBinaria extends ListaSimplesCircularBaseOrdenada {

    public ArvoreBinaria(long rai, String cri, int num) {

        super(rai, num, cri);
    }

    @Override
    protected void computeNodes(TreeMap<Long, Sketch> heap, WorldMap worldMap, long ref, boolean isBase) {

        dimSketch.x += Consts.spacingBig;

        //no need to check if ref is base node
        NoArvoreBinaria node = (NoArvoreBinaria) heap.get(ref);
        node.makeBase(cri);

        Vec2i pos = new Vec2i((posSketch.x + dimSketch.x), posSketch.y);

        node.computeSketch(heap, worldMap, pos);

        

        //its not efficient but it works
        computeHelper1(heap, ref);
        computeHelper2(heap, worldMap, ref, 0, 0);
    }

    private void computeHelper1(TreeMap<Long, Sketch> heap, long curr) {

        NoArvoreBinaria node = (NoArvoreBinaria) heap.get(curr);

        if (node == null)
            return;

        node.getDimEstimate(heap);

        computeHelper1(heap, node.esq);
        computeHelper1(heap, node.dir);
    }

    private void computeHelper2(TreeMap<Long, Sketch> heap, WorldMap worldMap, long curr, int x, int y) {

        NoArvoreBinaria node = (NoArvoreBinaria) heap.get(curr);

        if (node == null)
            return;

        node.computeSketch(heap, worldMap, new Vec2i(x, y));

        int xLeft;
        int yLeft;

        //TODO: call

        int xRight;
        int yRight;

        //TODO: call
    }

    private int getLeftSubTreeXOffSet(TreeMap<Long, Sketch> heap, long curr) {

        NoArvoreBinaria node = (NoArvoreBinaria) heap.get(curr);

        if (node == null)
            return 0;

        return (node.getDimSketch().x / 2 + Consts.spacingBig) + getLeftSubTreeXOffSet(heap, node.dir);
    }

    private int getRightSubTreeXOffSet(TreeMap<Long, Sketch> heap, long curr) {

        NoArvoreBinaria node = (NoArvoreBinaria) heap.get(curr);

        if (node == null)
            return 0;

        return (node.getDimSketch().x / 2 + Consts.spacingBig) + getRightSubTreeXOffSet(heap, node.esq);
    }

    @Override
    protected void drawNodes(Graphics2D graphics, TreeMap<Long, Sketch> heap, long ref, Vec2i tempOffSet) {

        NoArvoreBinaria node = (NoArvoreBinaria) heap.get(ref);
        drawHelper(graphics, heap, node, tempOffSet);
    }

    private void drawHelper(Graphics2D graphics, TreeMap<Long, Sketch> heapSketches, NoArvoreBinaria node, Vec2i tempOffSet) {

        if (node == null)
            return;

        node.drawSketch(graphics, null, tempOffSet);

        NoArvoreBinaria esq = (NoArvoreBinaria) heapSketches.get(node.esq);
        NoArvoreBinaria dir = (NoArvoreBinaria) heapSketches.get(node.dir);

        drawHelper(graphics, heapSketches, esq, tempOffSet);
        drawHelper(graphics, heapSketches, dir, tempOffSet);
    }
}
