package ui.arvores;

import math.Vec2i;
import model.HeapPrimitive;
import support.Consts;
import ui.Object;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.elements.HeapTextElement;
import ui.world.WorldMap;
import java.util.TreeMap;

public class NoArvoreBinaria extends Sketch {

    protected long esq;
    protected long ele;
    protected long dir;
    protected String str;

    public NoArvoreBinaria(long ele, long esq, long dir) {

        super();
        this.esq = esq;
        this.ele = ele;
        this.dir = dir;
        this.str = "";
    }

    public void makeBase(String cri) {

        dir = Consts.nullRef;
        str = cri;
        dimSketch = getStringMaxDim(str);
        dimSketch.y *= 2;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        if (dimSketch.isZero()) {
            getDimEstimate(heap);
        }

        Element e1 = new HeapTextElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, (dimSketch.y / 2)),
                str
        );

        Element e2 = new HeapRefElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y / 2)),
                new Vec2i((dimSketch.x / 2), (dimSketch.y / 2)),
                esq,
                true
        );

        Element e3 = new HeapRefElement(
                new Vec2i((posSketch.x + dimSketch.x / 2), (posSketch.y + dimSketch.y / 2)),
                new Vec2i((dimSketch.x / 2), (dimSketch.y / 2)),
                dir,
                true
        );

        elements.addLast(e1);
        elements.addLast(e2);
        elements.addLast(e3);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);
        e3.updateWorldMap(worldMap);
    }

    public void getDimEstimate(TreeMap<Long, Sketch> heap) {

        str = computeHelper1(heap);
        dimSketch = getStringMaxDim(str);
        dimSketch.y *= 2;
    }

    protected String computeHelper1(TreeMap<Long, Sketch> heap) {

        String str = "????";
        Object p = (Object) heap.get(ele);
        if (p != null) {
            HeapPrimitive hp = (HeapPrimitive) p.getFields().get("nome");
            if (hp != null) {
                str = hp.getStringValue();
            }
        }
        return str;
    }
}
