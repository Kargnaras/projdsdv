package ui.tabelas;

import math.Vec2i;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.elements.HeapTextElement;
import ui.world.WorldMap;
import java.util.TreeMap;

public class Entrada extends Sketch {

    protected long ass;
    protected String ati;

    public Entrada(long ass, String ati) {

        super();
        this.ass = ass;
        this.ati = ati;
    }

    public long getAss() {

        return ass;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch = getStringMaxDim(ati);

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                ass,
                false
        );

        Element e2 = new HeapTextElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y)),
                new Vec2i(dimSketch.x, dimSketch.y),
                ati
        );

        elements.addLast(e1);
        elements.addLast(e2);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);

        dimSketch.y *= 2;
    }
}
