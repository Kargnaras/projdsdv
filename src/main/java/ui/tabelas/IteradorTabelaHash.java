package ui.tabelas;

import math.Vec2i;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapTextElement;
import ui.world.WorldMap;
import java.util.TreeMap;

public class IteradorTabelaHash extends Sketch {

    protected String cor;
    protected String pro;

    public IteradorTabelaHash(int cor, int pro) {

        super();
        this.cor = String.valueOf(cor);
        this.pro = String.valueOf(pro);
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch = getStringMaxDim(cor);
        dimSketch.max(getStringMaxDim(pro));

        Element e1 = new HeapTextElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                cor
        );

        Element e2 = new HeapTextElement(
                new Vec2i((posSketch.x + dimSketch.x), posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                pro
        );

        elements.addLast(e1);
        elements.addLast(e2);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);

        dimSketch.x *= 2;
    }
}
