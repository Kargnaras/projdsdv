package ui.tabelas;

import math.Vec2i;
import model.HeapEntity;
import model.HeapPrimitive;
import support.Consts;
import ui.Sketch;
import ui.arrays.Array;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.elements.HeapTextElement;
import ui.world.WorldMap;
import java.awt.*;
import java.util.TreeMap;

public class TabelaHashPorSondagem extends Array {

    protected int inc;
    protected int num;
    protected int ina;
    protected long tab;
    protected String kin;

    public TabelaHashPorSondagem(String kin, long tab, int inc, int num, int ina) {

        super(null);
        this.kin = kin;
        this.tab = tab;
        this.inc = inc;
        this.num = num;
        this.ina = ina;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        arr = ((Array) heap.get(tab)).getArr();

        String[] strs = new String[4];
        strs[0] = "incremento: " + inc;
        strs[1] = "num elementos: " + num;
        strs[2] = "num elementos inativos: " + ina;
        strs[3] = "hashingIncremento: " + "hi"; //TODO: fix this

        Vec2i temDim = new Vec2i();

        for (String str : strs)
            temDim.max(getStringMaxDim(str));

        dimSketch = getStringMaxDim(String.valueOf(arr.length));

        int xDiff = - Math.min(0, (dimSketch.x - temDim.x));

        if (xDiff != 0) {

            Element e1 = new HeapTextElement(
                    new Vec2i(posSketch.x, posSketch.y),
                    new Vec2i(xDiff, (dimSketch.y * arr.length)),
                    ""
            );

            elements.addLast(e1);
            e1.updateWorldMap(worldMap);
        }

        for (int i = 0; i < arr.length; i++) {

            HeapPrimitive hp = (HeapPrimitive) arr[i];

            Element e1 = new HeapTextElement(
                    new Vec2i((posSketch.x + xDiff), (posSketch.y + dimSketch.y * i)),
                    new Vec2i(dimSketch.x, dimSketch.y),
                    String.valueOf(i)
            );

            Element e2 = new HeapRefElement(
                    new Vec2i((posSketch.x + xDiff + dimSketch.x), (posSketch.y + dimSketch.y * i)),
                    new Vec2i(Consts.spacingSmall, dimSketch.y),
                    Long.parseLong(hp.getStringValue()),
                    true
            );

            elements.addLast(e1);
            elements.addLast(e2);

            e1.updateWorldMap(worldMap);
            e2.updateWorldMap(worldMap);
        }

        dimSketch.x += (Consts.spacingSmall + xDiff);

        //--------------------------------------------------------------------------------------------------------------

        for (int i = 0; i < strs.length; i++) {

            Element e1 = new HeapTextElement(
                    new Vec2i(posSketch.x, (posSketch.y + dimSketch.y * arr.length + dimSketch.y * i)),
                    new Vec2i(dimSketch.x, dimSketch.y),
                    strs[i]
            );

            elements.addLast(e1);
            e1.updateWorldMap(worldMap);
        }

        dimSketch.y *= (arr.length + strs.length);

        computeHelper1(heap, worldMap);
    }

    @Override
    public void drawSketch(Graphics2D g2d, TreeMap<Long, Sketch> heap, Vec2i tempOffSet) {

        if (!isDrawn) {

            for (Element e : elements)
                e.drawElement(g2d, tempOffSet);

            for (HeapEntity he : arr) {

                Entrada entrada = (Entrada) heap.get(Long.valueOf(((HeapPrimitive) he).getStringValue()));

                if (entrada == null)
                    continue;

                entrada.drawSketch(g2d, heap, tempOffSet);
                Associacao associacao = (Associacao) heap.get(entrada.getAss());

                if (associacao != null)
                    associacao.drawSketch(g2d, heap, tempOffSet);
            }

            isDrawn = true;
        }
    }

    @Override
    protected void computeHelper1(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        int xOffSet = Math.max(1, arr.length / Consts.reason);

        dimSketch.x += Consts.spacingBig * xOffSet;

        Vec2i auxPos = new Vec2i((posSketch.x + dimSketch.x), posSketch.y);
        Vec2i auxDim = new Vec2i();

        for (HeapEntity he : arr) {

            Entrada entrada = (Entrada) heap.get(Long.valueOf(((HeapPrimitive) he).getStringValue()));

            if (entrada == null)
                continue;

            entrada.computeSketch(heap, worldMap, auxPos);
            Associacao associacao = (Associacao) heap.get(entrada.getAss());

            if (associacao != null && !associacao.isCompu()) {

                int x = entrada.getDimSketch().x + Consts.spacingBig;

                auxPos.x += x;

                associacao.computeSketch(heap, worldMap, auxPos);

                int w = Math.max(auxDim.x, x + associacao.getDimSketch().x);
                int h = Math.max(entrada.getDimSketch().y, associacao.getDimSketch().y) + Consts.spacingBig;

                auxPos.x -= x;
                auxPos.y += h;

                auxDim.x  = w;
                auxDim.y += h;
            }

            else {

                int w = Math.max(auxDim.x, entrada.getDimSketch().x);
                int h = entrada.getDimSketch().y + Consts.spacingBig;

                auxPos.y += h;

                auxDim.x  = w;
                auxDim.y += h;
            }
        }

        dimSketch.x += auxDim.x;
        dimSketch.y  = Math.max(dimSketch.y, auxDim.y);
    }
}
