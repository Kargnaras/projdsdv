package ui.tabelas;

import math.Vec2i;
import ui.Object;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapTextElement;
import ui.world.WorldMap;
import java.awt.*;
import java.util.TreeMap;

public class Associacao extends Sketch {

    protected String cha;
    protected long val;

    public Associacao(String cha, long val) {

        super();
        this.cha = cha;
        this.val = val;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        Object object = (Object) heap.get(val);

        if (object == null)
            return;

        dimSketch = getStringMaxDim(cha);

        object.computeSketch(heap, worldMap, new Vec2i((posSketch.x + dimSketch.x), posSketch.y));

        dimSketch.y = object.getDimSketch().y;

        Element e1 = new HeapTextElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                cha
        );

        elements.addLast(e1);

        e1.updateWorldMap(worldMap);

        dimSketch.x += object.getDimSketch().x;
    }

    @Override
    public void drawSketch(Graphics2D g2d, TreeMap<Long, Sketch> heap, Vec2i tempOffSet) {

        if (!isDrawn) {
            for (Element e : elements) { e.drawElement(g2d, tempOffSet); }
            Object object = (Object) heap.get(val);
            object.drawSketch(g2d, heap, tempOffSet);
            isDrawn = true;
        }
    }
}
