package ui;

import math.Vec2i;
import model.HeapEntity;
import model.HeapPrimitive;
import support.Consts;
import ui.elements.*;
import ui.world.WorldMap;
import java.util.Map;
import java.util.TreeMap;

public class Object extends Sketch {

    protected TreeMap<String, HeapEntity> fields;

    public Object(TreeMap<String, HeapEntity> fields) {

        super();
        this.fields = fields;
    }

    public TreeMap<String, HeapEntity> getFields() {

        return fields;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        computeHelper2((0), computeHelper1(fields), worldMap);

        dimSketch.y *= fields.size();
    }

    protected String[] computeHelper1(TreeMap<String, HeapEntity> fields) {

        int i = -1;
        String[] strs = new String[fields.size()];

        for (Map.Entry<String, HeapEntity> field : fields.entrySet())  {

            HeapPrimitive hp  = (HeapPrimitive) field.getValue();
            String        key = field.getKey() + ": " + hp.getStringValue();

            if (hp.isReference()) { key = field.getKey(); }

            dimSketch.max(getStringMaxDim(key));
            strs[++i] = key;
        }

        return strs;
    }

    protected void computeHelper2(int tempY, String[] strs, WorldMap worldMap) {

        int i = -1;
        int xOffSet = 0;

        for (Map.Entry<String, HeapEntity> field : fields.entrySet()) {

            HeapPrimitive hp = (HeapPrimitive) field.getValue();

            Element e1 = getTextElement(
                    new Vec2i(posSketch.x, (posSketch.y + tempY)),
                    new Vec2i(dimSketch.x, dimSketch.y),
                    strs[++i]
            );

            elements.addLast(e1);
            e1.updateWorldMap(worldMap);

            if (hp.isReference()) {

                Element e2 = getRefElement(
                        new Vec2i((posSketch.x + dimSketch.x) , (posSketch.y + tempY)),
                        new Vec2i(Consts.spacingSmall, dimSketch.y),
                        Long.parseLong(hp.getStringValue())
                );

                elements.addLast(e2);
                e2.updateWorldMap(worldMap);

                xOffSet = Consts.spacingSmall;
            }

            tempY += dimSketch.y;
        }

        dimSketch.x += xOffSet;
    }

    protected Element getTextElement(Vec2i pos, Vec2i dim, String str) {

        return new HeapTextElement(pos, dim, str);
    }

    protected Element getRefElement(Vec2i pos, Vec2i dim, long ref) {

        return new HeapRefElement(pos, dim, ref, false);
    }
}