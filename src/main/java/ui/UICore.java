package ui;

import backend.Trace;
import math.Vec2i;
import model.HeapEntity;
import support.Consts;
import ui.world.WorldMap;
import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

public class UICore extends JPanel implements Runnable {

    public enum Move { UP, DOWN, LEFT, RIGHT, NONE }

    private Move moveDir;
    private Thread drawThread;
    private final Vec2i offSet;
    private final Vec2i dimension;
    private boolean isInitialized = false;
    private final LinkedList<Frame> stack;
    private final TreeMap<Long, Sketch> heap;
    private final WorldMap worldMap;

    public UICore() {

        this.setLayout(null);
        this.setBackground(Consts.backgroundColor);

        this.moveDir = Move.NONE;
        this.offSet = new Vec2i();
        this.dimension = new Vec2i();
        this.worldMap = new WorldMap();

        this.heap = new TreeMap<>();
        this.stack = new LinkedList<>();
    }

    @Override
    public void run() {

        movementClock();
    }

    public void initAndDraw(Trace trace) {

        isInitialized = false;

        stack.clear();
        for (model.Frame frame : trace.stack)
            stack.addLast(new Frame(frame.name_, frame.this_, frame.fields));

        heap.clear();
        for (Map.Entry<Long, HeapEntity> entry : trace.heap.entrySet())
            heap.put(entry.getKey(), entry.getValue().makeSketch());

        worldMap.clear();

        dimension.clear();

        computeSketches();

        worldMap.computeMap();

        worldMap.computeConns(heap);

        offSet.clear();

        isInitialized = true;

        this.repaint();
    }

    public void moveContent(boolean up, boolean down, boolean left, boolean right) {

        Move prevMoveDir = moveDir;

        if (up)         { moveDir = Move.DOWN;  }
        else if (down)  { moveDir = Move.UP;    }
        else if (left)  { moveDir = Move.RIGHT; }
        else if (right) { moveDir = Move.LEFT;  }
        else            { moveDir = Move.NONE;  }

        if (prevMoveDir != moveDir) {

            if (prevMoveDir == Move.NONE) {

                drawThread = new Thread(this);
                drawThread.start();
            }

            else if (moveDir == Move.NONE) {

                try {
                    drawThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void paintComponent(Graphics graphics) {

        if (!isInitialized)
            return;

        super.paintComponent(graphics);

        Graphics2D g2d = (Graphics2D) graphics;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);

        g2d.setColor(Consts.backgroundColor);
        g2d.fillRect(0, 0, this.getSize().width, this.getSize().height);

        for (Frame frame : stack) {
            frame.drawSketch(g2d, null, offSet);
            for (Long ref : frame.refs) {
                Sketch sketch = heap.get(ref);
                sketch.drawSketch(g2d, heap, offSet);
            }
        }

        worldMap.drawConns(g2d, offSet);

        /*
        g2d.setColor(Color.GRAY);
        for (int i = 0; i <= worldMap.getMapDim().x; i++)
            g2d.drawLine(i * Consts.boxSide, 0, i * Consts.boxSide, this.getHeight());
        for (int j = 0; j <= worldMap.getMapDim().y; j++)
            g2d.drawLine(0, j * Consts.boxSide, this.getWidth(), j * Consts.boxSide);
         */

        resetDrawnFlags();
    }

    private void movementClock() {

        double now;
        double delta = 0;
        double ini   = System.currentTimeMillis();
        double then  = System.currentTimeMillis();

        while (moveDir != Move.NONE) {

            now   = System.currentTimeMillis();
            delta = delta + now - then;
            then  = now;

            while (delta > Consts.period) {

                delta -= Consts.period;

                update(((now - ini) / 1000d));
            }

            this.repaint();
        }
    }

    private void update(double time) {

        int cushion = Consts.spacingBig * Consts.boxSide;
        int increment = (int)(Consts.initialSpeed + 0.5d * Consts.acceleration * time * time);

        //it works, but what???
        int m = Consts.spacing * Consts.boxSide;

        switch (moveDir) {

            case UP:
                if ((offSet.y + dimension.y) > cushion)
                    offSet.y -= increment;
                break;

            case DOWN:
                if (offSet.y < (this.getHeight() - cushion - m))
                    offSet.y += increment;
                break;

            case LEFT:
                if ((offSet.x + dimension.x) > cushion)
                    offSet.x -= increment;
                break;

            case RIGHT:
                if (offSet.x < (this.getWidth() - cushion - m))
                    offSet.x += increment;
                break;
        }
    }

    private void resetDrawnFlags() {

        for (Frame frame : stack)
            frame.isDrawn = false;

        for (Map.Entry<Long, Sketch> entry : heap.entrySet())
            entry.getValue().isDrawn = false;
    }

    private void computeSketches() {

        Vec2i auxPos = new Vec2i();
        Vec2i cursor = new Vec2i(Consts.spacingSmall, Consts.spacingSmall);

        for (Frame frame : stack) {

            frame.computeSketch(heap, worldMap, cursor);
            auxPos.x = cursor.x + frame.getDimSketch().x + Consts.spacingBig;
            auxPos.y = cursor.y;

            for (Long ref : frame.refs) {

                Sketch sketch = heap.get(ref);

                if (!sketch.isCompu) {

                    sketch.computeSketch(heap, worldMap, auxPos);
                    auxPos.y += sketch.getDimSketch().y + Consts.spacingBig;

                    dimension.x = Math.max(dimension.x, (frame.getDimSketch().x + Consts.spacingBig + sketch.getDimSketch().x));
                }
            }

            cursor.y += Math.max((frame.getDimSketch().y + Consts.spacingBig), (auxPos.y - cursor.y));
        }

        dimension.y = cursor.y - Consts.spacingBig - Consts.spacingSmall;
        dimension.mul(Consts.boxSide);
    }
}

