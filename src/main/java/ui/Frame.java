package ui;

import math.Vec2i;
import model.HeapEntity;
import model.HeapPrimitive;
import support.Consts;
import ui.elements.*;
import ui.world.WorldMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

public class Frame extends Object {

    protected String name_;
    protected String this_;
    protected LinkedList<Long> refs;

    public Frame(String name_, String this_, TreeMap<String, HeapEntity> fields) {

        super(fields);
        this.name_ = name_;
        this.this_ = this_;
        this.refs = new LinkedList<>();

        for (Map.Entry<String, HeapEntity> entry : fields.entrySet()) {

            HeapPrimitive hp = (HeapPrimitive) entry.getValue();
            long ref = Long.parseLong(hp.getStringValue());
            if (ref != Consts.nullRef) { refs.addLast(ref); }
        }
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch.max(getStringMaxDim(name_));
        dimSketch.max(getStringMaxDim(this_));

        Element e1 = new StackNameElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                name_
        );

        Element e2 = getTextElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y)),
                new Vec2i(dimSketch.x, dimSketch.y),
                this_
        );

        elements.addLast(e1);
        elements.addLast(e2);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);

        computeHelper2((dimSketch.y * 2), computeHelper1(fields), worldMap);

        dimSketch.y *= (fields.size() + 2);
    }

    @Override
    protected Element getTextElement(Vec2i pos, Vec2i dim, String str) {

        return new StackTextElement(pos, dim, str);
    }

    @Override
    protected Element getRefElement(Vec2i pos, Vec2i dim, long ref) {

        return new StackRefElement(pos, dim, ref, true);
    }
}
