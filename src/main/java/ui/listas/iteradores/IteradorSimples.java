package ui.listas.iteradores;

import math.Vec2i;
import support.Consts;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.world.WorldMap;
import java.util.TreeMap;

public class IteradorSimples extends Sketch {

    protected long pro;
    protected long cur;

    public IteradorSimples(long pro, long cur) {

        super();
        this.pro = pro;
        this.cur = cur;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch.x = Consts.spacing * 2;
        dimSketch.y = Consts.spacing;

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(Consts.spacing, Consts.spacing),
                cur,
                false
        );

        Element e2 = new HeapRefElement(
                new Vec2i((posSketch.x + Consts.spacing), posSketch.y),
                new Vec2i(Consts.spacing, Consts.spacing),
                pro,
                false
        );

        elements.addLast(e1);
        elements.addLast(e2);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);
    }
}
