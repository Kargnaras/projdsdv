package ui.listas.iteradores;

import math.Vec2i;
import support.Consts;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.world.WorldMap;
import java.util.TreeMap;

public class IteradorDuplo extends Sketch {

    protected long curr;
    protected long antPri;
    protected long segUlt;

    public IteradorDuplo(long curr, long antPri, long segUlt) {

        super();
        this.curr = curr;
        this.antPri = antPri;
        this.segUlt = segUlt;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch.x = Consts.spacing * 3;
        dimSketch.y = Consts.spacing;

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(Consts.spacing, Consts.spacing),
                antPri,
                false
        );

        Element e2 = new HeapRefElement(
                new Vec2i((posSketch.x + Consts.spacing), posSketch.y),
                new Vec2i(Consts.spacing, Consts.spacing),
                curr,
                false
        );

        Element e3 = new HeapRefElement(
                new Vec2i((posSketch.x + Consts.spacing * 2), posSketch.y),
                new Vec2i(Consts.spacing, Consts.spacing),
                segUlt,
                false
        );

        elements.addLast(e1);
        elements.addLast(e2);
        elements.addLast(e3);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);
        e3.updateWorldMap(worldMap);
    }
}
