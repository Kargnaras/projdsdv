package ui.listas;

import math.Vec2i;
import support.Consts;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.elements.HeapTextElement;
import ui.world.WorldMap;
import java.util.TreeMap;

public class ListaDuplaCircularBaseNaoOrdenada extends ListaSimplesCircularBaseNaoOrdenada {

    public ListaDuplaCircularBaseNaoOrdenada(long bas, int num) {

        super(bas, Consts.nullRef, num);
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch = getStringMaxDim(num);

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                bas,
                false
        );

        Element e2 = new HeapTextElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y)),
                new Vec2i(dimSketch.x, dimSketch.y),
                num
        );

        elements.addLast(e1);
        elements.addLast(e2);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);

        dimSketch.y *= 2;

        computeNodes(heap, worldMap, bas, true);
    }
}
