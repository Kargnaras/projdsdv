package ui.listas;

import math.Vec2i;
import support.Consts;
import ui.Object;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.elements.HeapTextElement;
import ui.listas.nos.NoLista;
import ui.world.WorldMap;
import java.awt.*;
import java.util.TreeMap;

public class ListaSimplesNaoOrdenada extends Sketch {

    protected long   ini;
    protected long   fin;
    protected String num;

    public ListaSimplesNaoOrdenada(long ini, long fin, int num) {

        super();
        this.ini = ini;
        this.fin = fin;
        this.num = String.valueOf(num);
    }

    public String getNum() {

        return num;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch = getStringMaxDim(num);

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                fin,
                false
        );

        Element e2 = new HeapRefElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y)),
                new Vec2i(dimSketch.x, dimSketch.y),
                ini,
                false
        );

        Element e3 = new HeapTextElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y * 2)),
                new Vec2i(dimSketch.x, dimSketch.y),
                num
        );

        elements.addLast(e1);
        elements.addLast(e2);
        elements.addLast(e3);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);
        e3.updateWorldMap(worldMap);

        dimSketch.y *= 3;

        computeNodes(heap, worldMap, ini, false);
    }

    @Override
    public void drawSketch(Graphics2D g2d, TreeMap<Long, Sketch> heap, Vec2i tempOffSet) {

        if (!isDrawn) {
            for (Element e : elements) { e.drawElement(g2d, tempOffSet); }
            drawNodes(g2d, heap, ini, tempOffSet);
            isDrawn = true;
        }
    }

    protected void computeNodes(TreeMap<Long, Sketch> heap, WorldMap worldMap, long ref, boolean isBase) {

        NoLista node = (NoLista) heap.get(ref);

        if (node != null && !node.isCompu()) {

            dimSketch.x += Consts.spacingBig;
            if (isBase) { node.makeBase(); }
        }

        Vec2i auxPos = new Vec2i((posSketch.x + dimSketch.x), posSketch.y);
        Vec2i auxDim = new Vec2i();

        while (node != null && !node.isCompu()) {

            node.computeSketch(heap, worldMap, auxPos);

            Object object = (Object) heap.get(node.getEle());

            if (object != null && !object.isCompu()) {

                int y = node.getDimSketch().y + Consts.spacingBig;

                auxPos.y += y;

                object.computeSketch(heap, worldMap, auxPos);

                int w = Math.max(node.getDimSketch().x, object.getDimSketch().x) + Consts.spacingBig;
                int h = Math.max(auxDim.y, y + object.getDimSketch().y);

                auxPos.x += w;
                auxPos.y -= y;

                auxDim.x += w;
                auxDim.y  = h;
            }

            else {

                int w = node.getDimSketch().x + Consts.spacingBig;
                int h = Math.max(dimSketch.y, node.getDimSketch().y);

                auxPos.x += w;

                auxDim.x += w;
                auxDim.y  = h;
            }

            node = (NoLista) heap.get(node.getSeg());
        }

        dimSketch.x += auxDim.x;
        dimSketch.y  = Math.max(dimSketch.y, auxDim.y);
    }

    protected void drawNodes(Graphics2D g2d, TreeMap<Long, Sketch> heap, long curr, Vec2i tempOffSet) {

        NoLista node = (NoLista) heap.get(curr);

        while (node != null && !node.isDrawn()) {

            node.drawSketch(g2d, heap, tempOffSet);
            Object object = (Object) heap.get(node.getEle());

            if (object != null)
                object.drawSketch(g2d, null, tempOffSet);

            node = (NoLista) heap.get(node.getSeg());
        }
    }
}