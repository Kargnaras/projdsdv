package ui.listas;

import math.Vec2i;
import support.Consts;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.elements.HeapTextElement;
import ui.world.WorldMap;
import java.util.TreeMap;

public class ListaSimplesCircularBaseOrdenada extends ListaSimplesCircularBaseNaoOrdenada {

    protected String cri;

    public ListaSimplesCircularBaseOrdenada(long bas, int num, String cri) {

        super(bas, Consts.nullRef, num);
        this.cri = cri;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch = getStringMaxDim(cri);

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                bas,
                false
        );

        Element e2 = new HeapTextElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y)),
                new Vec2i(dimSketch.x, dimSketch.y),
                cri
        );

        Element e3 = new HeapTextElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y * 2)),
                new Vec2i(dimSketch.x, dimSketch.y),
                num
        );

        elements.addLast(e1);
        elements.addLast(e2);
        elements.addLast(e3);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);
        e3.updateWorldMap(worldMap);

        dimSketch.y *= 3;

        computeNodes(heap, worldMap, bas, true);
    }
}
