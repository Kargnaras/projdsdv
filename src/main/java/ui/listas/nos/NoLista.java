package ui.listas.nos;

import support.Consts;
import ui.Sketch;
import ui.world.WorldMap;
import java.util.TreeMap;

public class NoLista extends Sketch {

    protected long ant;
    protected long ele;
    protected long seg;

    public NoLista(long ant, long ele, long seg) {

        super();
        this.ant = ant;
        this.ele = ele;
        this.seg = seg;
    }

    public long getAnt() {

        return ant;
    }

    public long getEle() {

        return ele;
    }

    public long getSeg() {

        return seg;
    }

    public void makeBase() {

        ele = Consts.nullRef;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {}
}
