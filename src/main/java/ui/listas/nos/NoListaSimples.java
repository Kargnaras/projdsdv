package ui.listas.nos;

import math.Vec2i;
import support.Consts;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.world.WorldMap;
import java.util.TreeMap;

public class NoListaSimples extends NoLista {

    public NoListaSimples(long ele, long seg) {

        super(Consts.nullRef, ele, seg);
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch.x = Consts.spacing + Consts.spacingSmall;
        dimSketch.y = Consts.spacing;

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(Consts.spacing, Consts.spacing),
                ele,
                true
        );

        Element e2 = new HeapRefElement(
                new Vec2i((posSketch.x + Consts.spacing), posSketch.y),
                new Vec2i(Consts.spacingSmall, Consts.spacing),
                seg,
                false
        );

        elements.addLast(e1);
        elements.addLast(e2);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);
    }
}
