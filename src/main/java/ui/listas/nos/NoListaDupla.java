package ui.listas.nos;

import math.Vec2i;
import support.Consts;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.world.WorldMap;
import java.util.TreeMap;

public class NoListaDupla extends NoLista {

    public NoListaDupla(long ant, long ele, long seg) {

        super(ant, ele, seg);
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch.x = Consts.spacingBig;
        dimSketch.y = Consts.spacing;

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(Consts.spacingSmall, Consts.spacingSmall),
                ant,
                false
        );

        Element e2 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(Consts.spacingBig, Consts.spacing),
                ele,
                true
        );

        Element e3 = new HeapRefElement(
                new Vec2i((posSketch.x + Consts.spacingBig - Consts.spacingSmall), (posSketch.y + Consts.spacing - Consts.spacingSmall)),
                new Vec2i(Consts.spacingSmall, Consts.spacingSmall),
                seg,
                false
        );

        elements.addLast(e2);
        elements.addLast(e1);
        elements.addLast(e3);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);
        e3.updateWorldMap(worldMap);
    }
}
