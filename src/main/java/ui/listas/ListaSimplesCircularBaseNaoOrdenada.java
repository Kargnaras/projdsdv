package ui.listas;

import math.Vec2i;
import support.Consts;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.elements.HeapTextElement;
import ui.world.WorldMap;
import java.awt.*;
import java.util.TreeMap;

public class ListaSimplesCircularBaseNaoOrdenada extends ListaSimplesNaoOrdenada {

    protected long bas;

    public ListaSimplesCircularBaseNaoOrdenada(long bas, long fin, int num) {

        super(Consts.nullRef, fin, num);
        this.bas = bas;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch = getStringMaxDim(num);

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                bas,
                false
        );

        Element e2 = new HeapRefElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y)),
                new Vec2i(dimSketch.x, dimSketch.y),
                fin,
                false
        );

        Element e3 = new HeapTextElement(
                new Vec2i(posSketch.x, (posSketch.y + dimSketch.y * 2)),
                new Vec2i(dimSketch.x, dimSketch.y),
                num
        );

        elements.addLast(e1);
        elements.addLast(e2);
        elements.addLast(e3);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);
        e3.updateWorldMap(worldMap);

        dimSketch.y *= 3;

        computeNodes(heap, worldMap, bas, true);
    }

    @Override
    public void drawSketch(Graphics2D g2d, TreeMap<Long, Sketch> heap, Vec2i tempOffSet) {

        if (!isDrawn) {
            for (Element e : elements) { e.drawElement(g2d, tempOffSet); }
            drawNodes(g2d, heap, bas, tempOffSet);
            isDrawn = true;
        }
    }
}
