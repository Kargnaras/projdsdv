package ui.pilhas;

import math.Vec2i;
import support.Consts;
import ui.Object;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.world.WorldMap;
import java.awt.*;
import java.util.TreeMap;

public class Pilha extends Sketch {

    protected long top;

    public Pilha(long top) {

        super();
        this.top = top;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch.x = Consts.spacing;
        dimSketch.y = Consts.spacing;

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(dimSketch.x, dimSketch.y),
                top,
                false
        );

        elements.addLast(e1);

        e1.updateWorldMap(worldMap);

        computeNodes(heap, worldMap);
    }

    @Override
    public void drawSketch(Graphics2D g2d, TreeMap<Long, Sketch> heap, Vec2i tempOffSet) {

        super.drawSketch(g2d, heap, tempOffSet);

        NoPilha node = (NoPilha) heap.get(top);

        while (node != null && !node.isDrawn()) {

            node.drawSketch(g2d, null, tempOffSet);
            Object object = (Object) heap.get(node.getEle());

            if (object != null)
                object.drawSketch(g2d, null, tempOffSet);

            node = (NoPilha) heap.get(node.getSeg());
        }
    }

    protected void computeNodes(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        NoPilha node = (NoPilha) heap.get(top);

        if (node != null && !node.isCompu())
            dimSketch.x += Consts.spacingBig;

        Vec2i auxPos = new Vec2i((posSketch.x + dimSketch.x), posSketch.y);
        Vec2i auxDim = new Vec2i();

        while (node != null && !node.isCompu()) {

            node.computeSketch(heap, worldMap, auxPos);

            Object object = (Object) heap.get(node.getEle());

            if (object != null && !object.isCompu()) {

                int x = node.getDimSketch().x + Consts.spacingBig;

                auxPos.x += x;

                object.computeSketch(heap, worldMap, auxPos);

                int w = Math.max(auxDim.x, x + object.getDimSketch().x);
                int h = Math.max(node.getDimSketch().y, object.getDimSketch().y) + Consts.spacingBig;

                auxPos.x -= x;
                auxPos.y += h;

                auxDim.x  = w;
                auxDim.y += h;
            }

            else {

                int w = Math.max(auxDim.x, node.getDimSketch().x);
                int h = node.getDimSketch().y + Consts.spacingBig;

                auxPos.y += h;

                auxDim.x  = w;
                auxDim.y += h;
            }

            node = (NoPilha) heap.get(node.getSeg());
        }

        dimSketch.x += auxDim.x;
        dimSketch.y  = Math.max(dimSketch.y, auxDim.y);
    }
}
