package ui.pilhas;

import math.Vec2i;
import support.Consts;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.listas.nos.NoListaSimples;
import ui.world.WorldMap;
import java.util.TreeMap;

public class NoPilha extends NoListaSimples {

    public NoPilha(long ele, long seg) {

        super(ele, seg);
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        dimSketch.x = Consts.spacing;
        dimSketch.y = Consts.spacing + Consts.spacingSmall;

        Element e1 = new HeapRefElement(
                new Vec2i(posSketch.x, posSketch.y),
                new Vec2i(Consts.spacing, Consts.spacing),
                ele,
                false
        );

        Element e2 = new HeapRefElement(
                new Vec2i(posSketch.x, (posSketch.y + Consts.spacing)),
                new Vec2i(Consts.spacing, Consts.spacingSmall),
                seg,
                false
        );

        elements.addLast(e1);
        elements.addLast(e2);

        e1.updateWorldMap(worldMap);
        e2.updateWorldMap(worldMap);
    }
}
