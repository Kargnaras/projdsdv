package ui.arrays;

import math.Vec2i;
import model.HeapEntity;
import model.HeapPrimitive;
import support.Consts;
import ui.Object;
import ui.Sketch;
import ui.elements.Element;
import ui.elements.HeapRefElement;
import ui.elements.HeapTextElement;
import ui.world.WorldMap;
import java.awt.*;
import java.util.TreeMap;

public class Array extends Sketch {

    protected boolean hasRefs;
    protected HeapEntity[] arr;

    public Array(HeapEntity[] arr) {

        super();
        this.arr = arr;
        this.hasRefs = true;
    }

    public HeapEntity[] getArr() {

        return arr;
    }

    @Override
    protected void compute(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        hasRefs = ((HeapPrimitive) arr[0]).isReference();

        dimSketch = getStringMaxDim(String.valueOf(arr.length));

        for (int i = 0; i < arr.length; i++) {

            Element e1 = new HeapTextElement(
                    new Vec2i(posSketch.x, (posSketch.y + dimSketch.y * i)),
                    new Vec2i(dimSketch.x, dimSketch.y),
                    String.valueOf(i)
            );

            elements.addLast(e1);
            e1.updateWorldMap(worldMap);
        }

        if (hasRefs) {

            for (int i = 0; i < arr.length; i++) {

                HeapPrimitive hp = (HeapPrimitive) arr[i];

                Element e1 = new HeapRefElement(
                        new Vec2i((posSketch.x + dimSketch.x), (posSketch.y + dimSketch.y * i)),
                        new Vec2i(Consts.spacingSmall, dimSketch.y),
                        Long.parseLong(hp.getStringValue()),
                        true
                );

                elements.addLast(e1);
                e1.updateWorldMap(worldMap);
            }

            dimSketch.x += Consts.spacingSmall;
            dimSketch.y *= arr.length;

            computeHelper1(heap, worldMap);
            return;
        }

        Vec2i auxDim = new Vec2i();

        for (int i = 0; i < arr.length; i++) {

            HeapPrimitive hp = (HeapPrimitive) arr[i];
            auxDim.max(getStringMaxDim(hp.getStringValue()));
        }

        for (int i = 0; i < arr.length; i++) {

            HeapPrimitive hp = (HeapPrimitive) arr[i];

            Element e1 = new HeapTextElement(
                    new Vec2i((posSketch.x + dimSketch.x), (posSketch.y + dimSketch.y * i)),
                    new Vec2i(auxDim.x, dimSketch.y),
                    hp.getStringValue()
            );

            elements.addLast(e1);
            e1.updateWorldMap(worldMap);
        }

        dimSketch.x += auxDim.x;
        dimSketch.y *= arr.length;
    }

    protected void computeHelper1(TreeMap<Long, Sketch> heap, WorldMap worldMap) {

        int xOffSet = Math.max(1, arr.length / Consts.reason);

        dimSketch.x += Consts.spacingBig * xOffSet;

        Vec2i auxPos = new Vec2i((posSketch.x + dimSketch.x), posSketch.y);
        Vec2i auxDim = new Vec2i();

        for (int i = 0; i < arr.length; i++) {

            HeapPrimitive hp = (HeapPrimitive) arr[i];
            Object object = (Object) heap.get(Long.valueOf(hp.getStringValue()));

            if (object != null && !object.isCompu()) {

                object.computeSketch(heap, worldMap, auxPos);

                auxPos.y += object.getDimSketch().y + Consts.spacingBig;

                auxDim.x = Math.max(auxDim.x, object.getDimSketch().x);
                auxDim.y += object.getDimSketch().y + Consts.spacingBig;
            }

            else {

                auxPos.y += Consts.spacingBig;

                auxDim.y += Consts.spacingBig;
            }
        }

        dimSketch.x += auxDim.x;
        dimSketch.y += auxDim.y - Consts.spacingBig;
    }

    @Override
    public void drawSketch(Graphics2D g2d, TreeMap<Long, Sketch> heap, Vec2i tempOffSet) {

        if (!isDrawn) {
            for (Element e : elements) {
                e.drawElement(g2d, tempOffSet);
            }
            if (hasRefs) {
                for (int i = 0; i < arr.length; i++) {
                    HeapPrimitive hp = (HeapPrimitive) arr[i];
                    Sketch sketch = heap.get(Long.parseLong(hp.getStringValue()));

                    if (sketch != null)
                        sketch.drawSketch(g2d, null, tempOffSet);
                }
            }
            isDrawn = true;
        }
    }
}
