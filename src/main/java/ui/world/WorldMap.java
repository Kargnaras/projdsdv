package ui.world;

import math.Vec2i;
import support.Consts;
import java.awt.*;
import java.util.*;
import ui.Sketch;
import ui.world.connections.Conn;
import ui.world.connections.PosRef;

public class WorldMap {

    private Box[][] mapArr;
    private final Vec2i dimMap;
    private final LinkedList<Area> areas;
    private final LinkedList<PosRef> posRefs;
    private final LinkedList<Conn> conns;

    public WorldMap() {

        this.dimMap  = new Vec2i();
        this.areas   = new LinkedList<>();
        this.posRefs = new LinkedList<>();
        this.conns   = new LinkedList<>();
    }

    public Vec2i getMapDim() {

        return dimMap;
    }

    public void clear() {

        dimMap.clear();
        areas.clear();
        posRefs.clear();
        conns.clear();
    }

    public void addPosRef(Vec2i startPos, long finishKey, boolean overrideASTAR) {

        if (finishKey != Consts.nullRef)
            posRefs.push(new PosRef(startPos, finishKey, overrideASTAR));
    }

    public void addArea(Vec2i ini, Vec2i dim, Status status) {

        Vec2i last = ini.addNew(dim);

        dimMap.x = Math.max(dimMap.x, last.x);
        dimMap.y = Math.max(dimMap.y, last.y);

        areas.addLast(new Area(ini, last, status));
    }

    public void computeMap() {

        mapArr = new Box[dimMap.x][dimMap.y];

        for (int i = 0; i < dimMap.x; i++)
            for (int j = 0; j < dimMap.y; j++)
                mapArr[i][j] = new Box(new Vec2i(i, j));

        for (Area area : areas) {

            for (int i = area.first.x; i < area.last.x; i++)
                for (int j = area.first.y; j < area.last.y; j++)
                    mapArr[i][j].status = area.status;

            //--------------------------------------------------------

            for (int i = area.first.x - 1; i < area.last.x + 1; i++) {
                Box box = getAt(i, (area.first.y - 1));
                if (box != null)
                    box.status = Status.BORDER;
            }

            for (int i = area.first.x - 1; i <  area.last.x + 1; i++) {
                Box box = getAt(i, area.last.y);
                if (box != null)
                    box.status = Status.BORDER;
            }

            for (int j = area.first.y; j <  area.last.y; j++) {
                Box box = getAt((area.first.x - 1), j);
                if (box != null)
                    box.status = Status.BORDER;
            }

            for (int j = area.first.y; j <  area.last.y; j++) {
                Box box = getAt(area.last.x, j);
                if (box != null)
                    box.status = Status.BORDER;
            }
        }
    }

    public LinkedList<Box> getNeighborBoxes(Box curr) {

        LinkedList<Box> neighbors = new LinkedList<>();

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {

                //removes itself
                if (i == 0 && j == 0)
                    continue;

                //removes diagonals
                if (i == j || (i == -j)) {
                    continue;
                }

                int posX = curr.pos.x + i;
                int posY = curr.pos.y + j;

                //invalid x position
                if (posX < 0 || posX >= dimMap.x)
                    continue;

                //invalid y position
                if (posY < 0 || posY >= dimMap.y)
                    continue;

                Box neighbor = mapArr[posX][posY];

                //its a wall
                if (neighbor.status == Status.OCCUPIED)
                    continue;

                //cannot go to another object reference field
                if (curr.status == Status.FREE && neighbor.status == Status.REFERENCE)
                    continue;

                neighbors.push(neighbor);
            }
        }

        return neighbors;
    }

    public void computeConns(TreeMap<Long, Sketch> heap) {

        for (PosRef curr : posRefs) {

            Sketch ske = heap.get(curr.getFinishRef());
            Vec2i  posSke = ske.getPosSketch();
            Vec2i  dimSke = ske.getDimSketch();

            TreeMap<Long, Box> goals = new TreeMap<>();

            for (int i = posSke.x; i < posSke.x + dimSke.x; i++) {
                Box box = getAt(i, (posSke.y - 1));
                if (box != null)
                    goals.put(box.getKey(), box);
            }

            for (int i = posSke.x; i < posSke.x + dimSke.x; i++) {
                Box box = getAt(i, (posSke.y + dimSke.y + 1));
                if (box != null)
                    goals.put(box.getKey(), box);
            }

            for (int j = posSke.y; j < posSke.y + dimSke.y; j++) {
                Box box = getAt((posSke.x - 1), j);
                if (box != null)
                    goals.put(box.getKey(), box);
            }

            for (int j = posSke.y; j < posSke.y + dimSke.y; j++) {
                Box box = getAt((posSke.x + dimSke.x + 1), j);
                if (box != null)
                    goals.put(box.getKey(), box);
            }

            if (curr.overrideASTAR()) {

                Conn conn = new Conn(true);

                Vec2i v1 = new Vec2i(curr.getStartPos().x, curr.getStartPos().y);
                Vec2i v2 = manhattan(curr.getStartPos(), goals);

                conn.addPointLast(v1);
                conn.addPointLast(v2);

                conn.compute();
                conns.addLast(conn);

                continue;
            }

            Box ini = mapArr[curr.getStartPos().x][curr.getStartPos().y];

            Conn conn = solveASTAR(ini, goals);

            if (conn != null) {
                conn.compute();
                conns.addLast(conn);
            }

            for (int i = 0; i < dimMap.x; i++)
                for (int j = 0; j < dimMap.y; j++)
                    mapArr[i][j].cleanAstarData();
        }
    }

    public void drawConns(Graphics2D graphics, Vec2i offSet) {

        for (Conn conn : conns)
            conn.draw(graphics, offSet);
    }

    private Box getAt(int x, int y) {

        if (x < 0 || x >= dimMap.x)
            return null;

        if (y < 0 || y >= dimMap.y)
            return null;

        return mapArr[x][y];
    }

    private Conn solveASTAR(Box ini, TreeMap<Long, Box> goals) {

        ArrayList<Box> openSet = new ArrayList<>();

        ini.gScore = 0f;
        ini.fScore = manhattan(ini, goals);

        openSet.add(ini);

        while (!openSet.isEmpty()) {

            openSet.sort(fComparator);

            Box curr = openSet.remove(0);

            if (goals.containsKey(curr.getKey())) {

                return getConn(curr);
            }

            LinkedList<Box> neighbors = getNeighborBoxes(curr);

            while (!neighbors.isEmpty()) {

                Box neighbor = neighbors.removeFirst();

                float tentativeGScore = curr.gScore + neighbor.getCost();

                if (tentativeGScore < neighbor.gScore) {

                    neighbor.parent = curr;
                    neighbor.gScore = tentativeGScore;
                    neighbor.fScore = neighbor.gScore + manhattan(neighbor, goals);

                    if (!openSet.contains(neighbor)) {

                        openSet.add(neighbor);
                    }
                }
            }
        }

        return null;
    }

    private float manhattan(Box curr, TreeMap<Long, Box> goals) {

        float min = Float.MAX_VALUE;

        for (Map.Entry<Long, Box> goal : goals.entrySet()) {

            Box currGoal = goal.getValue();
            min = Math.min(min, Math.abs(curr.pos.x - currGoal.pos.x) + Math.abs(curr.pos.y - currGoal.pos.y));
        }

        return min;
    }

    private Vec2i manhattan(Vec2i pos, TreeMap<Long, Box> goals) {

        Vec2i out = new Vec2i();
        float min = Float.MAX_VALUE;

        for (Map.Entry<Long, Box> goal : goals.entrySet()) {

            Box currGoal = goal.getValue();

            float h = Math.abs(pos.x - currGoal.pos.x) + Math.abs(pos.y - currGoal.pos.y);

            if (h < min) {
                min = h;
                out.x = currGoal.getPos().x;
                out.y = currGoal.getPos().y;
            }
        }

        return out;
    }

    private Conn getConn(Box curr) {

        Conn conn = new Conn(false);

        while (curr != null) {

            conn.addPointFirst(curr.pos.cloneVec());
            curr.status = Status.CONNECTION;
            curr = curr.parent;
        }

        return conn;
    }

    private static final Comparator fComparator = (Comparator<Box>) (a, b) -> Float.compare(a.fScore, b.fScore);
}
