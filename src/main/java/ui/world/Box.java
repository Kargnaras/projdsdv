package ui.world;

import math.Vec2i;
import support.Utils;

public class Box {

    protected Box parent;
    protected float gScore;
    protected float fScore;
    protected Status status;
    protected final Vec2i pos;

    public Box(Vec2i pos) {

        this.pos = pos;
        this.parent = null;
        this.status = Status.FREE;
        this.gScore = Float.MAX_VALUE;
        this.fScore = Float.MAX_VALUE;
    }

    public Vec2i getPos() {

        return pos;
    }

    public long getKey() {

        return Utils.computeKey(pos.x, pos.y);
    }

    public void cleanAstarData() {

        gScore = Float.MAX_VALUE;
        fScore = Float.MAX_VALUE;
        parent = null;
    }

    public float getCost() {

        switch (status) {

            case FREE:
                return 1.0f;

            case BORDER:
            case REFERENCE:
                return 2.0f;

            case CONNECTION:
                return 6.0f;

            case OCCUPIED:
                return Float.MAX_VALUE;

            default:
                return 0.0f;
        }
    }

}