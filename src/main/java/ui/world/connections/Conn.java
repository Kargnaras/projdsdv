package ui.world.connections;

import math.Vec2i;
import support.Consts;
import support.Utils;
import java.awt.*;
import java.awt.geom.Path2D;
import java.util.LinkedList;

public class Conn {

    private boolean overridesASTAR;
    private final LinkedList<Vec2i> points;

    public Conn(boolean overridesASTAR) {

        this.overridesASTAR = overridesASTAR;
        this.points = new LinkedList<>();
    }

    public void addPointFirst(Vec2i point) {

        points.addFirst(point);
    }

    public void addPointLast(Vec2i point) {

        points.addLast(point);
    }

    public void compute() {

        for (Vec2i p : points)
            p.mul(Consts.boxSide);

        if (!overridesASTAR)
            for (Vec2i p : points)
                p.add(Consts.boxSide / 2);

        //TODO: remove unnecessary points
    }

    public void draw(Graphics2D graphics, Vec2i offset) {

        Vec2i prev = points.getFirst();
        Vec2i curr = points.getFirst();

        graphics.setColor(overridesASTAR ? Color.BLUE : Color.RED);

        for (Vec2i point : points) {

            prev = curr;
            curr = point;

            graphics.drawLine((prev.x + offset.x), (prev.y + offset.y), (curr.x + offset.x), (curr.y + offset.y));
        }

        Path2D p = Utils.makeTriangle((curr.x + offset.x), (curr.y + offset.y), (curr.x - prev.x), (curr.y - prev.y));
        graphics.fill(p);
    }
}
