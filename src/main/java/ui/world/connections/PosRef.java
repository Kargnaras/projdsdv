package ui.world.connections;

import math.Vec2i;

public class PosRef {

    private final Vec2i startPos;
    private final long finishRef;
    private final boolean overrideASTAR;

    public PosRef(Vec2i startPos, long finishRef, boolean overrideASTAR) {

        this.startPos = startPos;
        this.finishRef = finishRef;
        this.overrideASTAR = overrideASTAR;
    }

    public Vec2i getStartPos() {

        return startPos;
    }

    public long getFinishRef() {

        return finishRef;
    }

    public boolean overrideASTAR() {

        return overrideASTAR;
    }
}