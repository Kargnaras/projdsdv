package ui.world;

public enum Status {

    FREE,
    OCCUPIED,
    REFERENCE,
    BORDER,
    CONNECTION
}
