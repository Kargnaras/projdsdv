package ui.world;

import math.Vec2i;

public class Area {

    protected final Vec2i first;
    protected final Vec2i last;
    protected final Status status;

    public Area(Vec2i first, Vec2i last, Status status) {

        this.first = first;
        this.last = last;
        this.status = status;
    }
}
